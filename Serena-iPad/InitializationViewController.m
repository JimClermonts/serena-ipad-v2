//
//  InitializationViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 25-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "InitializationViewController.h"

@interface InitializationViewController ()

@end

@implementation InitializationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) checkWhichScreenIsFirst
{
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *screenchoice = [preferences objectForKey:@"screen"];
    
    if ([screenchoice isEqualToString:@"aat_passed"])
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"calibrationScreen"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    if ([screenchoice isEqualToString:@"calibration_passed"])
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"welcomeScreen"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"tokenCheck"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{    
    [self checkWhichScreenIsFirst];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
