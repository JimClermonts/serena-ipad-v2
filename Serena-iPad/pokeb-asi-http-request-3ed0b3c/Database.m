//  Database.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "Database.h"
#import "Frequency.h"

@implementation Database

NSUserDefaults *prefs;

-(void) setRightMultiplier:(float)rightMultiplier
{
    _rightMultiplier = rightMultiplier;
}

-(void) initDatabaseValues
{
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_DBid"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_0_DBid"]];    // +1?????

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_calibrationRes"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_0_calibrationRes"]];

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_freqency"]] == nil)
        [prefs setValue:@"125" forKey:[NSString stringWithFormat:@"Freq_0_freqency"]];

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_0_rightMultiplier"]];

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_0_leftMultiplier"]];

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_0_res"]];

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_savePos"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_0_savePos"]];

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_enabled"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_0_enabled"]];


    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_DBid"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_1_DBid"]];    // +1?????

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_calibrationRes"]] == nil)
        [prefs setValue:@"2130968584" forKey:[NSString stringWithFormat:@"Freq_1_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_freqency"]] == nil)
        [prefs setValue:@"500" forKey:[NSString stringWithFormat:@"Freq_1_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_rightMultiplier"]] == nil)
        [prefs setValue:@"0.000017" forKey:[NSString stringWithFormat:@"Freq_1_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_leftMultiplier"]] == nil)
        [prefs setValue:@"0.000019" forKey:[NSString stringWithFormat:@"Freq_1_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_res"]] == nil)
        [prefs setValue:@"2130968579" forKey:[NSString stringWithFormat:@"Freq_1_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_savePos"]] == nil)
        [prefs setValue:@"2" forKey:[NSString stringWithFormat:@"Freq_1_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_1_enabled"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_1_enabled"]];


    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_DBid"]] == nil)
        [prefs setValue:@"2" forKey:[NSString stringWithFormat:@"Freq_2_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_calibrationRes"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_2_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_freqency"]] == nil)
        [prefs setValue:@"750" forKey:[NSString stringWithFormat:@"Freq_2_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_2_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_2_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_2_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_savePos"]] == nil)
        [prefs setValue:@"3" forKey:[NSString stringWithFormat:@"Freq_2_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_2_enabled"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_2_enabled"]];


    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_DBid"]] == nil)
        [prefs setValue:@"3" forKey:[NSString stringWithFormat:@"Freq_3_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_calibrationRes"]] == nil)
        [prefs setValue:@"2130968581" forKey:[NSString stringWithFormat:@"Freq_3_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_freqency"]] == nil)
        [prefs setValue:@"1000" forKey:[NSString stringWithFormat:@"Freq_3_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_rightMultiplier"]] == nil)
        [prefs setValue:@"0.000011" forKey:[NSString stringWithFormat:@"Freq_3_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_leftMultiplier"]] == nil)
        [prefs setValue:@"0.000011" forKey:[NSString stringWithFormat:@"Freq_3_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_res"]] == nil)
        [prefs setValue:@"2130968576" forKey:[NSString stringWithFormat:@"Freq_3_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_savePos"]] == nil)
        [prefs setValue:@"4" forKey:[NSString stringWithFormat:@"Freq_3_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_3_enabled"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_3_enabled"]];


    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_DBid"]] == nil)
        [prefs setValue:@"4" forKey:[NSString stringWithFormat:@"Freq_4_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_calibrationRes"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_4_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_freqency"]] == nil)
        [prefs setValue:@"1500" forKey:[NSString stringWithFormat:@"Freq_4_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_4_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_4_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_4_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_savePos"]] == nil)
        [prefs setValue:@"5" forKey:[NSString stringWithFormat:@"Freq_4_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_4_enabled"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_4_enabled"]];

    
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_DBid"]] == nil)
        [prefs setValue:@"5" forKey:[NSString stringWithFormat:@"Freq_5_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_calibrationRes"]] == nil)
        [prefs setValue:@"2130968582" forKey:[NSString stringWithFormat:@"Freq_5_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_freqency"]] == nil)
        [prefs setValue:@"2000" forKey:[NSString stringWithFormat:@"Freq_5_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_rightMultiplier"]] == nil)
        [prefs setValue:@"0.000034" forKey:[NSString stringWithFormat:@"Freq_5_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_leftMultiplier"]] == nil)
        [prefs setValue:@"0.000028" forKey:[NSString stringWithFormat:@"Freq_5_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_res"]] == nil)
        [prefs setValue:@"2130968577" forKey:[NSString stringWithFormat:@"Freq_5_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_savePos"]] == nil)
        [prefs setValue:@"6" forKey:[NSString stringWithFormat:@"Freq_5_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_5_enabled"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_5_enabled"]];

    

    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_DBid"]] == nil)
        [prefs setValue:@"6" forKey:[NSString stringWithFormat:@"Freq_6_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_0_calibrationRes"]] == nil)
        [prefs setValue:@"2130968584" forKey:[NSString stringWithFormat:@"Freq_6_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_freqency"]] == nil)
        [prefs setValue:@"3000" forKey:[NSString stringWithFormat:@"Freq_6_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_6_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_6_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_6_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_savePos"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_6_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_6_enabled"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_6_enabled"]];

    

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_DBid"]] == nil)
        [prefs setValue:@"7" forKey:[NSString stringWithFormat:@"Freq_7_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_calibrationRes"]] == nil)
        [prefs setValue:@"2130968583" forKey:[NSString stringWithFormat:@"Freq_7_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_freqency"]] == nil)
        [prefs setValue:@"4000" forKey:[NSString stringWithFormat:@"Freq_7_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_7_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_7_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_7_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_savePos"]] == nil)
        [prefs setValue:@"7" forKey:[NSString stringWithFormat:@"Freq_7_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_7_enabled"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_7_enabled"]];

    

    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_DBid"]] == nil)
        [prefs setValue:@"8" forKey:[NSString stringWithFormat:@"Freq_8_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_calibrationRes"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_8_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_freqency"]] == nil)
        [prefs setValue:@"6000" forKey:[NSString stringWithFormat:@"Freq_8_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_8_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_8_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_8_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_savePos"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_8_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_8_enabled"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_8_enabled"]];


    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_DBid"]] == nil)
        [prefs setValue:@"9" forKey:[NSString stringWithFormat:@"Freq_9_DBid"]];    // +1?????
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_calibrationRes"]] == nil)
        [prefs setValue:@"2130968585" forKey:[NSString stringWithFormat:@"Freq_9_calibrationRes"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_freqency"]] == nil)
        [prefs setValue:@"8000" forKey:[NSString stringWithFormat:@"Freq_9_freqency"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_rightMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_9_rightMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_leftMultiplier"]] == nil)
        [prefs setValue:@"0.0" forKey:[NSString stringWithFormat:@"Freq_9_leftMultiplier"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_res"]] == nil)
        [prefs setValue:@"0" forKey:[NSString stringWithFormat:@"Freq_9_res"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_savePos"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_9_savePos"]];
    
    if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_9_enabled"]] == nil)
        [prefs setValue:@"1" forKey:[NSString stringWithFormat:@"Freq_9_enabled"]];
}

- (id) init {
    self = [super init];
    if (self != nil) {
        
        prefs = [NSUserDefaults standardUserDefaults];
        [self initDatabaseValues];
    }
    return self;
}

-(Frequency *) getFreqOfObject:(NSInteger)freqId
{
    Frequency *tempFreq = [[Frequency alloc] init];
    [tempFreq setDb_Id:freqId];
    
    if (freqId == 0)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 1)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 2)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 3)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 4)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 5)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 6)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 7)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 8)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }
    if (freqId == 9)
    {
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setCalibrationRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_freqency", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setFreq:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setRightMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", freqId]];
            float value = [storedvalue floatValue];
            [tempFreq setLeftMultiplier:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_res", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setRes:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_savePos", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setSavePos:value];
        }
        if ([prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]] != nil)
        {
            NSString *storedvalue = [prefs valueForKey:[NSString stringWithFormat:@"Freq_%i_enabled", freqId]];
            int value = [storedvalue intValue];
            [tempFreq setEnabled:value];
        }
    }

//    NSLog(@"getDBid %i",[tempFreq getDBid]);
//    NSLog(@"getCalibrationRes %i",[tempFreq getCalibrationRes]);
//    NSLog(@"getFreq %i",[tempFreq getFreq]);
//    NSLog(@"getRightMultiplier %f",[tempFreq getRightMultiplier]);
//    NSLog(@"getLeftMultiplier %f",[tempFreq getLeftMultiplier]);
//    NSLog(@"getRes %i",[tempFreq getRes]);
//    NSLog(@"getSavePos %i",[tempFreq getSavePos]);
//    NSLog(@"getEnabled %i",[tempFreq getEnabled]);
    return tempFreq;
}

- (BOOL)updateFreq:(Frequency *)freq
{
    NSString *dbID = [NSString stringWithFormat:@"%i", [freq getDBid]];
    NSString *calibrationRes = [NSString stringWithFormat:@"%i", [freq getCalibrationRes]];
    NSString *freqency = [NSString stringWithFormat:@"%i", [freq getFreq]];
    NSString *rightMultiplier = [NSString stringWithFormat:@"%f", [freq getRightMultiplier]];
    NSString *leftMultiplier = [NSString stringWithFormat:@"%f", [freq getLeftMultiplier]];
    NSString *res = [NSString stringWithFormat:@"%i", [freq getRes]];
    NSString *savePos = [NSString stringWithFormat:@"%i", [freq getSavePos]];
    NSString *enabled = [NSString stringWithFormat:@"%i", [freq getEnabled]];

    [prefs setValue:dbID forKey:[NSString stringWithFormat:@"Freq_%i_DBid", [freq getDBid]]];
    [prefs setValue:calibrationRes forKey:[NSString stringWithFormat:@"Freq_%i_calibrationRes", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_calibrationRes", [freq getDBid]]);
    [prefs setValue:freqency forKey:[NSString stringWithFormat:@"Freq_%i_freqency", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_freqency", [freq getDBid]]);
    [prefs setValue:rightMultiplier forKey:[NSString stringWithFormat:@"Freq_%i_rightMultiplier", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_rightMultiplier", [freq getDBid]]);
    [prefs setValue:leftMultiplier forKey:[NSString stringWithFormat:@"Freq_%i_leftMultiplier", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_leftMultiplier", [freq getDBid]]);
    [prefs setValue:res forKey:[NSString stringWithFormat:@"Freq_%i_res", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_res", [freq getDBid]]);
    [prefs setValue:savePos forKey:[NSString stringWithFormat:@"Freq_%i_savePos", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_savePos", [freq getDBid]]);
    [prefs setValue:enabled forKey:[NSString stringWithFormat:@"Freq_%i_enabled", [freq getDBid]]];
//    NSLog(@"Frequency_JIM4:  %@", [NSString stringWithFormat:@"Freq_%i_enabled", [freq getDBid]]);
    return true;
}

- (NSString *) getDate
{
    NSDate *todaysDate = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:1];
    NSDate *targetDate = [gregorian dateByAddingComponents:dateComponents toDate:todaysDate  options:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* dateString = [dateFormatter stringFromDate:targetDate];
    
    return dateString;
}

- (void)insertSettings:(NSString *)name:(NSString *)value
{
    NSString * date;
    date = [self getDate];
    [prefs setValue:value forKey:[NSString stringWithFormat:name]];
    [prefs setValue:date forKey:[NSString stringWithFormat:@"Calibration_date"]];
}

@end
