//
//  Database.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Frequency.h"

@interface Database : NSObject
{
    float _rightMultiplier;
    float _leftMultiplier;
    NSString *name;
}

- (NSString *)name;
- (id)init;
- (void)setName:(NSString *)input; //string



@property (nonatomic, strong) Frequency *glowinfo;
@property (nonatomic, strong) NSMutableArray * glowEventsInfoFav;


- (Frequency *) getFreqOfObject:(NSInteger)freqId;
- (BOOL)updateFreq:(Frequency *)freq;
- (long)insertSettings:(NSString *)name:(NSString *)value;

@end
