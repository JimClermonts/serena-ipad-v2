//
//  Login.h
//  Serena-ipad
//
//  Created by Student on 18-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkHelper.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Customer.h"
#import "PlaySound.h"
#import "Database.h"

@interface Login : NSObject
//Login response statuses
enum {
	RequestSuccess,
    InvalidCredentialsError,
    ConnectionError
	
};
typedef	uint32_t FinishedRequestStatus;

enum{
    RequestTypeLogin,
    RequestTypeCheckToken,
    RequestTypeSavePersonData
};
typedef uint32_t RequestTypes;
- (id) init;
-(ASIFormDataRequest*)CreateLoginRequestWithStoredAccount;
-(ASIFormDataRequest*)CreateLoginRequestWithUsername:(NSString*) username andPassword:(NSString*) password;
-(FinishedRequestStatus)HandleFinishedLoginRequest:(ASIFormDataRequest*)request;
-(FinishedRequestStatus)HandleFinishedCheckTokenRequest:(ASIFormDataRequest*)request withToken:(NSString*)token;
-(ASIFormDataRequest*)CreateCheckTokenRequestWithToken:(NSString*)token;
-(ASIFormDataRequest*)CreateSavePersonDataRequestWithCustomer:(Customer*)customer;
-(FinishedRequestStatus)HandleFinishedSavePersonDataRequest:(ASIFormDataRequest*)request;
//Please use this when there are no incomplete customers (otherwise requests will fail)
-(void)UploadSavedPersonDataBacklog;
-(BOOL)IsValidEmailAddress:(NSString*)email;
@end
