//
//  Login.m
//  Serena-ipad
//
//  This class communicates with the server to login with an account.
//
//  Created by Bart van Drongelen on 18-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "Login.h"

@implementation Login

NSUserDefaults *prefs;
NSString * usernameKey = @"username";
NSString * passwordKey = @"password";
NSString * tokenKey = @"token";
NSString * firstNameKey = @"firstname";
NSString * lastNameKey = @"lastname";
NSString * fullNameKey = @"name";
NSString * dateOfBirthKey = @"dateofbirth";
NSString * emailKey = @"email";
NSString * newsLetterKey = @"newsletter";

//NSString * loginUrl = @"http://samenhoren.nl/clarityproducts/tablet/login";
//NSString * tokenUrl = @"http://samenhoren.nl/clarityproducts/tablet/token";
//NSString * saveDataUrl = @"http://samenhoren.nl/clarityproducts/tablet/save_data";

NSString * loginUrl = @"http://samenhoren.nl/batterybenelux/index.php/tablet/login";
NSString * tokenUrl = @"http://samenhoren.nl/batterybenelux/index.php/tablet/token";
NSString * saveDataUrl = @"http://samenhoren.nl/batterybenelux/index.php/tablet/save_data";
NetworkHelper *networkHelper;
NSLocale *nlLocale;
PlaySound* playSound;


- (id) init
{
    self = [super init];
    prefs = [NSUserDefaults standardUserDefaults];
    networkHelper = [[NetworkHelper alloc] init];
    nlLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"nl_NL"];
    playSound = [[PlaySound alloc ]init];
    
    //NSLog(@"%ld", (long)[[[NSDate alloc]init] timeIntervalSince1970]);
    return self;
}


-(ASIFormDataRequest*)CreateLoginRequestWithUsername:(NSString*) username andPassword:(NSString*) password
{
    NSString* token = [prefs stringForKey:@"token"];
    if (token == nil || token.length == 0)
    {
        NSLog(@"Invalid Token!");
        /*return nil;*/
    }
    if (/*[networkHelper getConnectedToInternet] &&*/ username != nil && password != nil)
    {
        
        //TODO: Input sanitization
        NSURL * url = [NSURL URLWithString:loginUrl];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setTag:RequestTypeLogin];
        [request addPostValue:username forKey:usernameKey];
        [request addPostValue:password forKey:passwordKey];
        [request addPostValue:token forKey:tokenKey];
        [request addPostValue:@"1.0" forKey:@"tablet_version"]; // we need to give application version, for now forcing 1.0
        [request.userInfo setValue:token forKey:tokenKey];
        [request.userInfo setValue:username forKey:usernameKey];
        [request.userInfo setValue:password forKey:passwordKey];
        return request;
    }
    else
    {
        NSLog(@"Login failed.");
        return nil;
        
    }
    
}
-(ASIFormDataRequest*)CreateLoginRequestWithStoredAccount
{
    NSString* savedusername = [prefs stringForKey:usernameKey];
    NSString* savedpassword = [prefs stringForKey:passwordKey];
    
    
    if (savedusername != NULL && savedpassword!= NULL)
    {
        return [self CreateLoginRequestWithUsername:savedusername andPassword:savedpassword];
        
    }
    return nil;
}
-(ASIFormDataRequest*)CreateCheckTokenRequestWithToken:(NSString*)token
{
    
    if (![networkHelper getConnectedToInternet])
    {
        NSLog(@"Error: Not connected to internet");
        //return nil;
    }
    if (/*[networkHelper getConnectedToInternet] &&*/ token != nil && token.length != 0)
    {
        
        //TODO: Input sanitization
        NSURL * url = [NSURL URLWithString:tokenUrl];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setTag:RequestTypeCheckToken];
        [request addPostValue:token forKey:tokenKey];
        [request.userInfo setValue:token forKey:tokenKey];
        return request;
    }
    else
    {
        NSLog(@"Error creating token.");
        return nil;
        
    }
}
-(FinishedRequestStatus)HandleFinishedCheckTokenRequest:(ASIFormDataRequest*)request withToken:(NSString*)token
{
    
        /* Server sends no response data (according to original source), using status codes instead*/
        int status = [request responseStatusCode];
        FinishedRequestStatus returnStatus;
        switch (status)
        {
            case 200:
                
                //Success! Saving to user prefs
                [prefs setObject:token forKey:tokenKey];
                [prefs synchronize];
                returnStatus = RequestSuccess;
                break;
            case 401:
                NSLog(@"Incorrect Token");
                returnStatus = InvalidCredentialsError;
                break;
            default:
                NSLog(@"Error connecting to server");
                returnStatus = ConnectionError;
                break;
        }
        
        return returnStatus;
        
    
}
-(FinishedRequestStatus)HandleFinishedLoginRequest:(ASIFormDataRequest*)request
{
        /* Server sends no response data (according to original source), using status codes instead
        NSData *responseData = [request responseData];
        NSError* error;
        
        // JSON is NSArray:
        NSArray *backendJSONMessage = [NSJSONSerialization JSONObjectWithData:responseData
                                                                      options:NSJSONReadingMutableContainers
                                                                        error:&error];
        */
        int status = [request responseStatusCode];
        FinishedRequestStatus returnStatus;
        switch (status)
        {
            case 200:
                
                //Success! Saving to user prefs
                [prefs setObject:[[request userInfo] objectForKey:usernameKey] forKey:usernameKey];
                [prefs setObject:[[request userInfo] objectForKey:passwordKey] forKey:passwordKey];
                [prefs synchronize];
                returnStatus = RequestSuccess;
                break;
            case 401:
                NSLog(@"Incorrect login string");
                returnStatus = InvalidCredentialsError;
                break;
            default:
                NSLog(@"Error connecting to server");
                returnStatus = ConnectionError;
                break;
        }
        
        return returnStatus;
}
-(ASIFormDataRequest*)CreateSavePersonDataRequestWithCustomer:(Customer*)customer
{
    NSString* savedusername = [prefs stringForKey:usernameKey];
    NSString* savedpassword = [prefs stringForKey:passwordKey];
    NSString* token = [prefs stringForKey:tokenKey];
    NSString* firstname = [customer getCustomerName];
    NSString* lastname = [customer getCustomerSurName];
    NSDate * birthdate = [customer getDateOfBirth];
    NSString * email = [customer getCustomerEmail];
    Boolean wantToReceiveNewsLetter = [customer getWantToReceiveNewsLetter];
    NSString * newsLetter = @"1"; //default = newsletter


    if(wantToReceiveNewsLetter)
    {
       newsLetter = @"1";
    }
    else
    {
        newsLetter = @"0";
        NSLog(@"ik wil geen nieuwsbrief");
    }
    //NSString* transformedString = [NSString stringWithFormat:@"%@", newsLetter];
    
    if (savedusername.length == 0 && savedpassword.length == 0)
    {
        NSLog(@"Error: No valid username or password found in userprefs. Please login first.");
        return nil;
    }
    if (![networkHelper getConnectedToInternet])
    {
        NSLog(@"Error: Not connected to internet");
        //return nil;
    }
    if (/*[networkHelper getConnectedToInternet] &&*/ firstname.length != 0 && lastname.length != 0 && birthdate != nil && email.length != 0)
    {
        NSString* fullname = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
//        NSString* dateofbirthstring = [NSString stringWithFormat:@"%ld", (long)[birthdate timeIntervalSince1970]]; //Unix time
        NSString *birthdateofCustomer;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy"];
        birthdateofCustomer = [formatter stringFromDate:birthdate];
        NSString* dateofbirthstring = [NSString stringWithFormat:@"%@", birthdateofCustomer];
        
        //TODO: Input sanitization
        NSURL * url = [NSURL URLWithString:saveDataUrl];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
        [request setTag:RequestTypeSavePersonData];
        [request addPostValue:token forKey:tokenKey];
        [request addPostValue:savedusername forKey:usernameKey];
        [request addPostValue:savedpassword forKey:passwordKey];
        [request addPostValue:newsLetter forKey:@"contact"];  //This is the newsletter!
        [request addPostValue:fullname forKey:fullNameKey];
        [request addPostValue:dateofbirthstring forKey:dateOfBirthKey];
        [request addPostValue:email forKey:emailKey];
        [request addPostValue:@"nl" forKey:@"lang"];
        [request addPostValue:[NSString stringWithFormat:@"%ld", (long)[[[NSDate alloc]init] timeIntervalSince1970]] forKey: @"time"];
        
        NSMutableArray* frequenciesused = [playSound getFrequencies];
        for (NSInteger i = 0; i < 10; i++)
        {
            [request addPostValue:@"0" forKey:[NSString stringWithFormat:@"freq_%d_l", i]];
            [request addPostValue:@"0" forKey:[NSString stringWithFormat:@"freq_%d_r", i]];
        }
        if ([frequenciesused count] > 0)
        {
            for (NSInteger i = 0; i < [frequenciesused count]; i++ )
            {
                Frequency* freq = [frequenciesused objectAtIndex:i];
                NSString * leftEarKey = [NSString stringWithFormat:@"freq_%d_l",[freq getSavePos]];
                [request setPostValue:[NSString stringWithFormat:@"%d", [[[customer getLeftEarResults] objectAtIndex:i] integerValue]] forKey:leftEarKey];
                NSString * rightEarKey = [NSString stringWithFormat:@"freq_%d_r",[freq getSavePos]];
                [request setPostValue:[NSString stringWithFormat:@"%d", [[[customer getRightEarResults] objectAtIndex:i] integerValue]] forKey:rightEarKey];
            }
        }
        
        return request;
    }
    else
    {
        NSLog(@"Error creating token.");
        return nil;
        
    }
}
-(FinishedRequestStatus)HandleFinishedSavePersonDataRequest:(ASIFormDataRequest*)request;
{
    
        int status = [request responseStatusCode];
        FinishedRequestStatus returnStatus;
        switch (status)
        {
            case 200:
            
            //Success! Saving to user prefs
            returnStatus = RequestSuccess;
            break;
        case 401:
            NSLog(@"Incorrect login string");
            returnStatus = InvalidCredentialsError;
            break;
        default:
            NSLog(@"Error connecting to server");
            returnStatus = ConnectionError;
            break;
        }
    
    return returnStatus;
   
}
-(BOOL)IsValidEmailAddress:(NSString*)email
{
    NSRange atSignRange = [email rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"@"]];
    if (atSignRange.location != NSNotFound && atSignRange.length == 1)
    {
        NSString * emailDomain = [email substringFromIndex:(atSignRange.location+1)];
        NSRange dotRange = [emailDomain rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
        if (dotRange.location != NSNotFound)
        {
            return YES;
        }
    }
    return NO;
}
-(void)UploadSavedPersonDataBacklog
{
    NSInteger customersSavedCount = [prefs integerForKey:@"highestId"]+1;
    for (int i = 0; i < customersSavedCount; i++)
    {
        NSString * customerKey = [NSString stringWithFormat:@"Customer%d", i];
        prefs = [NSUserDefaults standardUserDefaults];
        NSData * encodedCustomer = [prefs objectForKey:customerKey];
        Customer* customer = (Customer *)[NSKeyedUnarchiver unarchiveObjectWithData: encodedCustomer];
        if (![customer hasBeenUploaded] && [customer isComplete])
        {
            ASIFormDataRequest* request = [self CreateSavePersonDataRequestWithCustomer:customer];
            request.userInfo = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%d", i] forKey:@"customer"];
            
            [request setDelegate:self];
            [request startAsynchronous];
        }
    }
}
-(void) requestFinished:(ASIHTTPRequest *) request
{
    NSString * customerId = [request.userInfo objectForKey:@"customer"];
    if (customerId != nil)
    {
        //This customer has been uploaded succesfully!
        switch ([request responseStatusCode])
        {
            case 200:
            {
                NSString * customerKey = [NSString stringWithFormat:@"Customer%d", [customerId integerValue]];
                prefs = [NSUserDefaults standardUserDefaults];
                NSData * encodedCustomer = [prefs objectForKey:customerKey];
                Customer* customer = (Customer *)[NSKeyedUnarchiver unarchiveObjectWithData: encodedCustomer];
                
                [customer setHasBeenUploaded:YES];
                
                NSData *reEncodedCustomer = [NSKeyedArchiver archivedDataWithRootObject:customer];
                [prefs setObject:reEncodedCustomer forKey:customerKey];
                [prefs synchronize];
                break;
            }
            default:
            {
                NSLog(@"Request sent for customer %@ has been denied by the server", customerId);
                break;
            }
    
        }

    }
    else
    {
        NSLog(@"An invalid request has been sent to the login.m requestFinished method. Has the delegate been set properly?");
    }
}
-(void) requestFailed:(ASIHTTPRequest *) request
{
    NSString * customerId = [request.userInfo objectForKey:@"customer"];
    if (customerId != nil)
    {
        NSLog(@"Request sent for customer %@ failed with an connection error", customerId);
    }
    else
    {
        NSLog(@"An invalid request has been sent to the login.m requestFailed method. Has the delegate been set properly?");
    }

}
@end
