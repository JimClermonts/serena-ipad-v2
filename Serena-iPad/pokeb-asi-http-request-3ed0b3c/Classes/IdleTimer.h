//
//  IdleTimer.h
//  Serena-iPad
//
//  Created by Bart van Drongelen on 4/25/13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>
#define MAXIDLETIMESECONDS 10
#define FINISHED_TEST_SOUND 11
#define ACTIVE NO

@interface IdleTimer : NSObject

//Universal static method to reset the idletimer in a view.
//For now moved to view due to event handling issues

//+ (void)ResetIdleTimer:(NSTimer*)idletimer;
@end
