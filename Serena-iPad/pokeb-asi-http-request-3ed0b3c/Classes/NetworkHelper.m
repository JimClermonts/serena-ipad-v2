//
//  Networking.m
//  Serena-ipad
//
//  Created by Bart van Drongelen on 18-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "NetworkHelper.h"


@implementation NetworkHelper

Reachability *reachability;
NetworkStatus remoteHostStatus;

-(NetworkStatus) getCurrentNetworkStatus
{
    return remoteHostStatus;
}
-(BOOL) getConnectedToInternet
{
    return [reachability isReachableViaWWAN];
}
-(id)init
{
    self = [super init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(networkChanged:) name:kReachabilityChangedNotification object:nil];

    reachability = [Reachability reachabilityForInternetConnection];
    //[reachability startNotifier];
    return self;
    
}
//- (void)networkChanged:(NSNotification *)notification
//{
    
//    remoteHostStatus = [reachability currentReachabilityStatus];
   
//}

@end
