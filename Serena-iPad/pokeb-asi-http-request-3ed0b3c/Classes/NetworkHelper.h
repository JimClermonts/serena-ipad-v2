//
//  Networking.h
//  Serena-ipad
//
//  Created by Student on 18-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"


@interface NetworkHelper : NSObject


-(NetworkStatus) getCurrentNetworkStatus;
-(BOOL) getConnectedToInternet;
-(id)init;
@end
