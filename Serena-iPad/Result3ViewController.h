//
//  Result3ViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Result3ViewController : UIViewController
- (IBAction)webshopButton:(id)sender;
- (IBAction)closeButton:(id)sender;

@end
