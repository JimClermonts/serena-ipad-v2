//
//  HearingTestViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "PlaySound.h"
#import "Customer.h"
#import "FinishedTestViewController.h"

@interface HearingTestViewController : UIViewController
//Required constants for testing
#define VOICE_HEARINGTEST_INTRO  19
#define VOICE_HEARINGTEST_INTRO_FIRST  6
#define VOICE_HEARINGTEST_LEFT_EAR  11
#define VOICE_HEARINGTEST_RIGHT_EAR  12
#define VOICE_HEARINGTEST_FINISHED  4
#define PLAY_SOUND_TIMEOUT  4.5
#define DEFAULT_FREQ_DELAY  1.5
#define DEFAULT_DB          55
#define MAX_DB              75
#define MIN_DB              20

//Defines what the user heard
typedef enum {
    BIG_FALSE = 1,
    BIG_TRUE = 2,
    SMALL_FALSE = 3,
    SMALL_TRUE = 4
} UserSoundState;

//Right or left ear
//NOTE: might need to be changed to -1 and 1 due to AVFoundations implementation
//typedef enum {
//    LEFT_EAR,
//    RIGHT_EAR
//} WhichEar;

//Sound strength values in DB
typedef enum {
    FIRST_DB = 10,
    SECOND_DB = 5
} SoundStrength;

typedef enum {
    VOICE02_IS_PLAYING,
    VOICE02_IS_DONE_PLAYING,
    VOICE01_IS_PLAYING,
    VOICE01_IS_DONE_PLAYING,
    VOICE06_IS_PLAYING,
    VOICE06_IS_DONE_PLAYING,
    VOICE07_IS_PLAYING,
    VOICE07_IS_DONE_PLAYING,
    BTN_PRESSED_FOR_FIRST_TIME,
    BTN_PRESSED_NOT_FIRST_TIME_AND_VOICE01_STILL_PLAYING,
    SOUND_TEST_IS_STARTED
} HearingTestState;

typedef enum {
    TEST_NOT_STARTED,
    TEST_IN_PROGRESS,
    TEST_PAUSED_OR_FINISHED
} TestState;

@property (weak, nonatomic) IBOutlet UIProgressView *rightEarProgressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *leftEarProgressBar;
@property(nonatomic) NSString* Name;
@property(nonatomic) NSString* Email;
@property(nonatomic) NSInteger customerId;
@property (weak, nonatomic) IBOutlet UIButton *soundButton;

- (IBAction)hearSoundButton:(id)sender;
- (IBAction)restartTestButton:(id)sender;

@end
