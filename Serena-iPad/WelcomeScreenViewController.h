//
//  WelcomeScreenViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "IdleTimer.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface WelcomeScreenViewController : UIViewController <UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView *scroll;
}
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
- (IBAction)continueButtonActivated:(id)sender;


@end
