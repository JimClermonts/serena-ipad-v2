//
//  main.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 18-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
