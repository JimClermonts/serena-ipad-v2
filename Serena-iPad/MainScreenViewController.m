//
//  MainScreenViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "MainScreenViewController.h"

@interface MainScreenViewController ()

@end

@implementation MainScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToStartTest
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"startTestScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)onlineShopButton:(id)sender {
}

- (IBAction)hearingTestButton:(id)sender {
    
    [self goToStartTest];
}
@end
