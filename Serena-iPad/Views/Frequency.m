//
//  Frequency.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "Frequency.h"

@implementation Frequency

-(NSInteger) getDBid
{
    return db_id;
}

-(void) setDb_Id:(NSInteger)db_ID
{
    db_id = db_ID;
}

-(NSInteger) getFreq
{
    return freq;
}

-(void) setFreq:(NSInteger)frequency
{
    freq = frequency;
}

-(void) setRightMultiplier:(float)rightmultiplier
{
    rightMultiplier = rightmultiplier;
}

-(float) getRightMultiplier
{
    return rightMultiplier;
}


-(void) setLeftMultiplier:(float)leftmultiplier
{
    leftMultiplier = leftmultiplier;
}

-(float) getLeftMultiplier
{
    return leftMultiplier;
}

-(NSInteger) getRes
{
    return res;
}

-(void) setRes:(NSInteger)resvalue
{
    res = resvalue;
}

-(NSInteger)getCalibrationRes
{
    return calibrationRes;
}

-(void) setCalibrationRes:(NSInteger)calibrationres
{
    calibrationRes = calibrationres;
}

-(NSInteger) getSavePos
{
    return savePos;
}

-(void) setSavePos:(NSInteger)savepos
{
    savePos = savepos;
}

-(void) setEnabled:(NSInteger)Enabled
{
    enabled = Enabled;
}

-(NSInteger) getEnabled
{
    return enabled;
}

@end
