//
//  Frequency.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Frequency : NSObject
{
    NSInteger db_id;
    NSInteger freq;
    float rightMultiplier;
    float leftMultiplier;
    NSInteger res;
    NSInteger calibrationRes;
    NSInteger savePos;
    NSInteger enabled;
}

-(NSInteger) getDBid;
-(void) setDb_Id:(NSInteger)db_id;
-(NSInteger) getFreq;
-(void) setFreq:(NSInteger)freq;
-(float) getRightMultiplier;
-(void) setRightMultiplier:(float)rightmultiplier;
-(float) getLeftMultiplier;
-(void) setLeftMultiplier:(float)leftmultiplier;
-(NSInteger) getRes;
-(void) setRes:(NSInteger)res;
-(NSInteger) getCalibrationRes;
-(void) setCalibrationRes:(NSInteger)calibrationres;
-(NSInteger) getSavePos;
-(void) setSavePos:(NSInteger)savepos;
-(NSInteger) getEnabled;
-(void) setEnabled:(NSInteger)Enabled;

@end
