//
//  FinishedTestViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "FinishedTestViewController.h"

@interface FinishedTestViewController ()

@end

@implementation FinishedTestViewController
NSString * saveKey;
Customer * currentCustomer;
NSUserDefaults * prefs;
UIActivityIndicatorView * activityView;
Login* loginHelper;
BOOL requestDone = NO;
BOOL timerAlreadyActivated = NO;
AVAudioPlayer *voicePlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
}
- (void) requestFinished:(ASIHTTPRequest *) request
{
    //Send finished request to the Login class, it will be handled there.
    [activityView stopAnimating];
    FinishedRequestStatus returnStatus = [loginHelper HandleFinishedSavePersonDataRequest:request];
    
    //Execute action according to response.
    
    switch (returnStatus) {
        case RequestSuccess:
            [currentCustomer setHasBeenUploaded:YES];
            
            
            break;
        case InvalidCredentialsError:
        {
            break;
        }
        case ConnectionError:
        {
            break;
        }
        default:
            NSLog(@"ReturnStatus is leeg. Werkt HandleFinishedRequest wel goed?");
            break;
    }
    requestDone = YES;
    if (timerAlreadyActivated)
    {
        [self saveAndGoToNextView];
        
    }
    
    
}
-(void)saveAndGoToNextView
{
    NSInteger fletcherIndex;
    NSData *encodedCustomer = [NSKeyedArchiver archivedDataWithRootObject:currentCustomer];
    [prefs setObject:encodedCustomer forKey:saveKey];
    [prefs synchronize];
    fletcherIndex = [currentCustomer getFletcherIndex];
    if ([currentCustomer hasBeenUploaded])
    {
        //We have internet, so we try and upload the customer backlog while where at it
        [loginHelper UploadSavedPersonDataBacklog];
    }
    NSLog(@"%d", fletcherIndex);
    if (fletcherIndex < 25)
    {
        [self goToResult1];
    }
    else if (fletcherIndex >= 25 && fletcherIndex < 35)
    {
        [self goToResult2];
    }
    else
    {
        [self goToResult3];
    }
}
-(void) requestFailed:(ASIHTTPRequest *) request
{
    //[activityView stopAnimating];
    NSLog(@"Login: failed login attempt.");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet"
                                                    message:@"Er is geen internet. Uw gegevens zijn lokaal opgeslagen, en zullen worden verzonden zodra er weer verbinding is."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert setTag:1];
    [alert show];
    
    
}

-(void)activateVoice:(NSString *)voice
{
    NSString * stringPath;

    if ([voice isEqualToString:@"voice_08"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_08" ofType:@"mp3"];
    }
    
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    NSError *error;
    voicePlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    [voicePlayer setVolume:1];
    [voicePlayer play];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self ResetIdleTimer];
    loginHelper = [[Login alloc]init];
    saveKey = [NSString stringWithFormat:@"Customer%d", self.customerId];
    prefs = [NSUserDefaults standardUserDefaults];
    NSData * encodedCustomer = [prefs objectForKey:saveKey];
    currentCustomer = (Customer *)[NSKeyedUnarchiver unarchiveObjectWithData: encodedCustomer];
    [currentCustomer setIsComplete:YES];
    [self activateVoice:@"voice_08"];
    
     ASIHTTPRequest *request = [loginHelper CreateSavePersonDataRequestWithCustomer:currentCustomer];
     if (request != nil)
     {
         NSLog(@"if (request != nil)");
         //[activityView startAnimating];
         //[self.view addSubview:activityView];
         [request setDelegate:self];
         [request startAsynchronous];
     }
     [NSTimer scheduledTimerWithTimeInterval:FINISHED_TEST_SOUND target:self selector:@selector(nextViewTimerElapsed) userInfo:nil repeats:NO];
}

- (void)nextViewTimerElapsed
{
    
    if(requestDone)
    {
        [self saveAndGoToNextView];
    }
    else
    {
        timerAlreadyActivated = YES;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToResult1
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"result1Screen"];
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)goToResult2
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"result2Screen"];
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)goToResult3
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"result3Screen"];
    [self.navigationController pushViewController:controller animated:YES];
}

NSTimer* idleTimer;
- (void)ResetIdleTimer{
    if (ACTIVE)
    {
        if (!idleTimer) {
            idleTimer = [NSTimer scheduledTimerWithTimeInterval:FINISHED_TEST_SOUND
                                                         target:self
                                                       selector:@selector(idleTimerExceeded)
                                                       userInfo:nil
                                                        repeats:NO];
        }
        else {
            if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < FINISHED_TEST_SOUND-1.0) {
                [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:FINISHED_TEST_SOUND]];
            }
        }
    }
    
}
//Triggers when view has been idling for too long
- (void)idleTimerExceeded {
    if (self.isViewLoaded && self.view.window)
    {
        idleTimer = nil;
        [self ResetIdleTimer];
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"advertisement"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}
//Reset timer when something happens in UI
- (UIResponder *)nextResponder {
    [self ResetIdleTimer];
    return [super nextResponder];
}

- (IBAction)shopButton:(id)sender {
    [self showAreYouSurePopup];
}

- (void) showAreYouSurePopup
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Webshop"
                                                      message:@"U verlaat de app en gaat naar onze shop, weet u het zeker?"
                                                     delegate:self
                                            cancelButtonTitle:@"Nee"
                                            otherButtonTitles:@"Ja", nil];
    [message setTag:0];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (alertView.tag)
    {
        case 0:
        {
            NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
            if([title isEqualToString:@"Ja"])
            {
                NSURL *myURL = [NSURL URLWithString:@"cibapp://"];
                [[UIApplication sharedApplication] openURL:myURL];
            }
            break;
        }
        case 1:
        {
            requestDone = YES;
            if (timerAlreadyActivated)
            {
                [self saveAndGoToNextView];
                
            }
            break;
        }
        default:
            NSLog(@"Alertview not recognized");
            break;
    }
    
}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}
@end
