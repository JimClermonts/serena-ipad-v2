//
//  MainScreenViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainScreenViewController : UIViewController
- (IBAction)onlineShopButton:(id)sender;
- (IBAction)hearingTestButton:(id)sender;

@end
