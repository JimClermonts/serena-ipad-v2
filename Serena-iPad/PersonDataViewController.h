//
//  PersonDataViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "IdleTimer.h"
#import "Login.h"
#import "HearingTestViewController.h"
#import "Customer.h"

@interface PersonDataViewController : UIViewController <UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView *scroll;
}
- (IBAction)shopButton:(id)sender;
- (IBAction)nextButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *namePersonField;
@property (weak, nonatomic) IBOutlet UITextField *mailPersonField;
@property (weak, nonatomic) IBOutlet UITextField *surnamePersonField;
@property (weak, nonatomic) IBOutlet UITextField *datePersonField;
@property (weak, nonatomic) IBOutlet UISwitch *newsLetterSwitch;

@end
