//
//  StartTestViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "StartTestViewController.h"

@interface StartTestViewController ()

@end

@implementation StartTestViewController

NSTimer* idleTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)goToPersonDataView
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"personData"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewDidLoad
{
//    TODO
//    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"kioskapp//"]]) {
//        NSLog(@"jsdjshd");
//    }
//    else
//    {
//        NSLog(@"hoi jong");
//        
//    }
    
    [super viewDidLoad];
    [self ResetIdleTimer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continueButton:(id)sender {

    [self goToPersonDataView];
}

- (IBAction)shopButton:(id)sender {
    [self showAreYouSurePopup];
}

- (void) showAreYouSurePopup
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Webshop"
                                                      message:@"U verlaat de app en gaat naar onze shop, weet u het zeker?"
                                                     delegate:self
                                            cancelButtonTitle:@"Nee"
                                            otherButtonTitles:@"Ja", nil];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Ja"])
    {
        NSURL *myURL = [NSURL URLWithString:@"cibapp://"];
        [[UIApplication sharedApplication] openURL:myURL];
    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    if (!url) {  return NO; }
    
    NSString *URLString = [url absoluteString];
    [[NSUserDefaults standardUserDefaults] setObject:URLString forKey:@"url"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return YES;
}

- (void)ResetIdleTimer{
    if (ACTIVE)
    {
        if (!idleTimer) {
            idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAXIDLETIMESECONDS
                                                         target:self
                                                       selector:@selector(idleTimerExceeded)
                                                       userInfo:nil
                                                        repeats:NO];
        }
        else {
            if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < MAXIDLETIMESECONDS-1.0) {
                [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:MAXIDLETIMESECONDS]];
            }
        }
    }
    
}
//Triggers when view has been idling for too long
- (void)idleTimerExceeded {
    if (self.isViewLoaded && self.view.window)
    {
        idleTimer = nil;
        [self ResetIdleTimer];
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"advertisement"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
//Reset timer when something happens in UI
- (UIResponder *)nextResponder {
    [self ResetIdleTimer];
    return [super nextResponder];
}
@end
