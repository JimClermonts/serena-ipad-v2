//
//  PersonDataViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "PersonDataViewController.h"

#define MEASURED_DB_BTN_SCREEN_OFFSET 130

@interface PersonDataViewController ()
{    
}
@end

@implementation PersonDataViewController

@synthesize namePersonField;
@synthesize mailPersonField;
@synthesize surnamePersonField;
@synthesize datePersonField;
@synthesize newsLetterSwitch;

UIDatePicker * datePicker;
Login * loginHelper;
NSDate * currentdate;
UIActivityIndicatorView *activityView;
NSTimer * idleTimer;
NSString * firstname;
NSString * lastname;
NSDate * dateofbirth;
NSString * email;
Boolean wantToReceiveNewsLetter;
Boolean futureevent;
Customer * customer;
NSUserDefaults * prefs;
NSInteger offsetForKeyBoard;
bool stillEditing;
AVAudioPlayer *voicePlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)initDatePicker
{
    datePicker = [[UIDatePicker alloc] init];
    
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker addTarget:self action:@selector(pickerValueChanged) forControlEvents:UIControlEventValueChanged];
    currentdate = [[NSDate alloc]init];
    [datePicker setMaximumDate:currentdate];
    [datePersonField setInputView:datePicker];    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    loginHelper = [[Login alloc] init];
    activityView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center=self.view.center;
    
    [self initDatePicker];
    self.namePersonField.delegate = self;
    self.surnamePersonField.delegate = self;
    self.datePersonField.delegate = self;    
    self.mailPersonField.delegate = self;
    offsetForKeyBoard = MEASURED_DB_BTN_SCREEN_OFFSET;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    stillEditing = false;
    wantToReceiveNewsLetter = false;
    [self activateVoice:@"voice_00"];
    futureevent = false;
}

-(void)activateVoice:(NSString *)voice
{
    NSString * stringPath;
    
    if ([voice isEqualToString:@"voice_00"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_00" ofType:@"mp3"];
        NSURL *url = [NSURL fileURLWithPath:stringPath];
        NSError *error;
        voicePlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
        [voicePlayer setVolume:1];
        [voicePlayer play];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToHearingView
{
    HearingTestViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"hearingTest"];
    controller.Name = [NSString stringWithFormat:@"%@ %@", firstname, lastname];
    controller.Email = email;
    controller.customerId = [customer getCustomerId];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)shopButton:(id)sender {
    [self showAreYouSurePopup];
}

- (void) showAreYouSurePopup
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Webshop"
                                                      message:@"U verlaat de app en gaat naar onze shop, weet u het zeker?"
                                                     delegate:self
                                            cancelButtonTitle:@"Nee"
                                            otherButtonTitles:@"Ja", nil];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Ja"])
    {
        NSURL *myURL = [NSURL URLWithString:@"cibapp://"];
        [[UIApplication sharedApplication] openURL:myURL];
    }
}

- (IBAction)nextButton:(id)sender {
    firstname =  namePersonField.text;
    lastname = surnamePersonField.text;
    dateofbirth = datePicker.date;
    email = mailPersonField.text;
    
    //TODO: String sanitization required here?
    if (email.length != 0 && ![loginHelper IsValidEmailAddress:email])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ongeldig Emailadres"
                                                        message:@"Het emailadres dat is ingevoerd is geen geldig emailadres. Controleer of het ingevoerde emailadres klopt."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if (firstname.length != 0 && lastname.length != 0 && dateofbirth != nil && email.length != 0)
    {
        if (newsLetterSwitch.on)
        {
            wantToReceiveNewsLetter = true;
        }
        else
        {
            wantToReceiveNewsLetter = false;
        }
        
        customer = [[Customer alloc]initwithCustomerName:firstname SurName:lastname Email:email andDateOfBirth:dateofbirth andWantsToReceiveNewsLetter:wantToReceiveNewsLetter];
     prefs = [NSUserDefaults standardUserDefaults];
     NSString * saveKey = [NSString stringWithFormat:@"Customer%d", [customer getCustomerId]];
     NSData *encodedCustomer = [NSKeyedArchiver archivedDataWithRootObject:customer];
     [prefs setObject:encodedCustomer forKey:saveKey];
        [prefs synchronize];
     [self goToHearingView];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missende gegevens"
                                                        message:@"Niet alle velden zijn ingevuld. Probeer opnieuw."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

//NSTimer* idleTimer;
//- (void)ResetIdleTimer{
//    if (ACTIVE)
//    {
//        if (!idleTimer) {
//            idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAXIDLETIMESECONDS
//                                                         target:self
//                                                       selector:@selector(idleTimerExceeded)
//                                                       userInfo:nil
//                                                        repeats:NO];
//        }
//        else {
//            if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < MAXIDLETIMESECONDS-1.0) {
//                [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:MAXIDLETIMESECONDS]];
//            }
//        }
//    }
//    
//}
////Triggers when view has been idling for too long
//- (void)idleTimerExceeded {
//    if (self.isViewLoaded && self.view.window)
//    {
//        idleTimer = nil;
//        [self ResetIdleTimer];
//        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"advertisement"];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    
//}
////Reset timer when something happens in UI
//- (UIResponder *)nextResponder {
//    [self ResetIdleTimer];
//    return [super nextResponder];
//}

-(void)pickerValueChanged
{
    NSString * birthdate;
    
    NSRange spaceRange = [datePicker.date.description rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];

    int intervall = (int) [currentdate timeIntervalSinceDate: datePicker.date] / 60;
    
    if (intervall < 1200 && intervall > 0)
    {
        futureevent = true;
        NSDate *newDate1 = [datePicker.date dateByAddingTimeInterval:60*60*24*1];
        birthdate = [newDate1.description substringToIndex:spaceRange.location];
    }
    else
    {
        if (futureevent)
        {
            NSDate *newDate1 = [datePicker.date dateByAddingTimeInterval:60*60*24*1];
            birthdate = [newDate1.description substringToIndex:spaceRange.location];
        }
        else
        {
            birthdate = [datePicker.date.description substringToIndex:spaceRange.location];
        }
    }
    
    datePersonField.text = birthdate; // set text to date description
}


#pragma mark Screen offset functions
-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if (offsetForKeyBoard == MEASURED_DB_BTN_SCREEN_OFFSET)
    {
        stillEditing = true;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        CGRect rect = scroll.frame;
        
        rect.origin.y -= offsetForKeyBoard;
        rect.size.height += offsetForKeyBoard;
        scroll.frame = rect;
        offsetForKeyBoard = 0;
    }
    
    stillEditing = true;
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)sender
{
    [self setOffsetScreenBack];
}

- (void)keyboardDidHide:(NSNotification *)aNotification {
    stillEditing = false;
    [self setOffsetScreenBack];
}

-(void)setOffsetScreenBack
{
    if (offsetForKeyBoard == 0 && stillEditing == false)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
        CGRect rect = scroll.frame;
        rect.origin.y += MEASURED_DB_BTN_SCREEN_OFFSET;
        rect.size.height -= MEASURED_DB_BTN_SCREEN_OFFSET;
        scroll.frame = rect;
        [UIView commitAnimations];
        offsetForKeyBoard = MEASURED_DB_BTN_SCREEN_OFFSET;
    }
}
@end
