//
//  HearingTestViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "HearingTestViewController.h"

@interface HearingTestViewController ()

@end

@implementation HearingTestViewController
#pragma mark fields
//Timers required for test
NSTimer * soundTimer;
NSTimer * practiceTimer;
NSTimer * randomPause;
NSTimer * disableButtonTimer;
NSTimer * waitForVoiceToFinishTimer;
NSTimer * waitForVoiceIntroToFinishTimer;
NSTimer * waitForVoice06;
NSTimer * nextEarVoice;
NSTimer * voiceTestIsFinished;

NSUserDefaults * prefs;
//NSInteger freqCount = 10; //Possibly not needed anymore?
TestState currentTestState = TEST_NOT_STARTED;
HearingTestState hearingTestState;
NSInteger testAction = 0;
EarToPlaySoundOn  currentEar = LEFT_EAR;
NSInteger currentSound = 0;
NSInteger currentDecibel = DEFAULT_DB;
NSString* saveKey;
PlaySound *soundPlayer;
Customer* currentCustomer;
NSInteger maxFreqId;
AVAudioPlayer *voicePlayer;
BOOL pausedButtonResponse = NO;
bool rightEarIsDone;

#pragma mark Initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //rightEarIsDone = false;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    soundPlayer = [[PlaySound alloc] init];
    //Personal data check (Properties to be set in h file)
    if (self.Name == nil || self.Email == nil)
    {
        NSLog(@"Please set Name and Email property before loading this view!");
    }
    maxFreqId = [soundPlayer getMaxFreqId];
    
    //sp/vo?
    
    //Instantiate progressbars (without animation)
    //Color settings have been moved to the storyboard.
    //NOTE: color values in android app make no sense, using custom colors for now
    [_rightEarProgressBar setProgress:0 animated:NO];
    [_leftEarProgressBar setProgress:0 animated:NO];
    
    //[_soundButton setBackgroundImage:[UIImage imageNamed:@"img_button_verder.png"] forState:UIControlStateNormal];
    saveKey = [NSString stringWithFormat:@"Customer%d", self.customerId];
    prefs = [NSUserDefaults standardUserDefaults];
    NSData * encodedCustomer = [prefs objectForKey:saveKey];
    currentCustomer = (Customer *)[NSKeyedUnarchiver unarchiveObjectWithData: encodedCustomer];
    
    [self activateVoice:@"voice_02"];
    hearingTestState = VOICE02_IS_PLAYING;
    waitForVoiceIntroToFinishTimer = [NSTimer scheduledTimerWithTimeInterval:VOICE_HEARINGTEST_INTRO_FIRST target:self selector:@selector(playHearingTestIntroVoice) userInfo:nil repeats:NO];
}

#pragma mark HearingTestMethods
-(void)randomPlay
{
    //starts timer to check if user missed this sound
    soundTimer = [NSTimer scheduledTimerWithTimeInterval:PLAY_SOUND_TIMEOUT target:self selector:@selector(userMissedSound) userInfo:nil repeats:NO];
    [soundPlayer PlaySoundNow];
}

-(void)nextFreq
{
    [self saveFrequencyData];
    if (currentSound < [[soundPlayer getFrequencies] count] - 1)
    {
        currentSound++;
        currentDecibel = DEFAULT_DB;
        switch (currentEar)
        {
            case LEFT_EAR:
                [self setLeftEarProgressToSoundNr:currentSound];
                break;
            case RIGHT_EAR:
                [self setRightEarProgressToSoundNr:currentSound];
                break;
            default:
                NSLog(@"Unknown or invalid ear value, unable to set progress bar!");
                break;
        }
        [self playSound];
    }
    else
    {
        if (currentEar == LEFT_EAR)
        {
            //rightEarIsDone = true; // This is redundant, checks are already in place
            [_leftEarProgressBar setProgress:1.0 animated:YES];
            hearingTestState = VOICE07_IS_PLAYING;
            [self activateVoice:@"voice_07"];
            nextEarVoice = [NSTimer scheduledTimerWithTimeInterval:VOICE_HEARINGTEST_RIGHT_EAR target:self selector:@selector(nextEar) userInfo:nil repeats:NO];
        }
        else
        {
            [self nextEar];
        }
    }
}
//Button press callback are now two methods.
//Called when user presses button and test hasn't started yet
-(void)startTest
{
    currentEar = LEFT_EAR; //for clarity
    currentSound = 0;
    currentTestState = TEST_IN_PROGRESS;
    testAction = 0;
    [_soundButton setBackgroundImage:[UIImage imageNamed:@"img_button_hoorgeluid.png"] forState:UIControlStateNormal];
    [self playSound];
}
//Called when user thinks he heard a sound and presses the button
-(void)userHeardSound
{
    [self stopTimers];
    [soundPlayer StopSound];
    
    //Port from original code, with different naming
    switch (testAction)
    {
        case BIG_FALSE:
            testAction = SMALL_TRUE;
            currentDecibel -= SECOND_DB;
            [self playSound];
            break;
        case SMALL_FALSE:
            //Go to next frequency
            [self nextFreq];
            break;
        case BIG_TRUE:
            testAction = BIG_TRUE;
            currentDecibel -= SECOND_DB;
            [self playSound];
            break;
        case SMALL_TRUE:
            testAction = SMALL_FALSE;
            currentDecibel -= SECOND_DB;
            [self playSound];
            break;
        case 0:
            testAction = BIG_TRUE;
            currentDecibel -= FIRST_DB;
            [self playSound];
            break;
        default:
            NSLog(@"Error: Invalid value in testAction found (userHeardSound)");
            break;
    }
}
//Called when sound timer expires (user has missed sound)
-(void)userMissedSound
{
    [self stopTimers];
    [soundPlayer StopSound];
    
    //Port from original code, with different naming
    switch (testAction)
    {
        case BIG_FALSE:
            testAction = BIG_FALSE;
            currentDecibel += FIRST_DB;
            [self playSound];
            break;
        case SMALL_FALSE:
            testAction = SMALL_FALSE;
            currentDecibel += SECOND_DB;
            [self playSound];
            break;
        case BIG_TRUE:
            testAction = SMALL_FALSE;
            currentDecibel += SECOND_DB;
            [self playSound];
            break;
        case SMALL_TRUE:
            testAction = SMALL_FALSE;
            currentDecibel += SECOND_DB;
            [self playSound];
            break;
        case 0:
            testAction = BIG_FALSE;
            currentDecibel += FIRST_DB;
            [self playSound];
            break;
        default:
            NSLog(@"Error: Invalid value in testAction found (userMissedSound)");
            break;
            
    }
}
//Play the next sound for the test
-(void)playSound
{
    if (currentTestState == TEST_IN_PROGRESS)
    {
        //if currentdecibel gets out of bounds, jump to next frequency
        if (currentDecibel > MAX_DB || currentDecibel < MIN_DB)
        {
            [self nextFreq];
        }
        else
        {
            [self stopTimers];
            //[self disableButton]; //disable button for a while until sound plays
            pausedButtonResponse = YES;
            //ARC4random is a built in random generator, better than rand() apparently
            long randomlong = arc4random_uniform(1000)+300;
            float timerseconds = randomlong/1000; //milliseconds -> seconds
            
            [soundPlayer SetupSoundWithFrequency:currentSound WithDecibel:currentDecibel OnEar:currentEar];
            //1.5 second delay for ogg file, disable
            disableButtonTimer =[NSTimer scheduledTimerWithTimeInterval:DEFAULT_FREQ_DELAY target:self selector:@selector(unPauseButtonResponse) userInfo:nil repeats:NO];
            //[self enableButton];
            //Start random pause timer
            randomPause = [NSTimer scheduledTimerWithTimeInterval:timerseconds target:self selector:@selector(randomPlay) userInfo:nil repeats:NO];
        }
    }
    else
    {
        [self stopTimers];
        [soundPlayer StopSound];
    }
}
-(void)unPauseButtonResponse
{
    pausedButtonResponse = NO;
}
-(void)nextEar
{
    hearingTestState = SOUND_TEST_IS_STARTED;

    switch (currentEar)
    {
        case LEFT_EAR: //Only Left Ear complete, moving to right
            //Force progress bar to 100% (in case of rounding errors)
            [_leftEarProgressBar setProgress:1.0 animated:YES];
            currentEar = RIGHT_EAR;
            currentSound = 0;
            currentDecibel = DEFAULT_DB;
            //TODO: play voice
            [self playSound];
            
            break;
        case RIGHT_EAR: //Left and right complete, finished test
            //Force progress bar to 100% (in case of rounding errors)
            [_rightEarProgressBar setProgress:1.0 animated:YES];
            currentTestState = TEST_PAUSED_OR_FINISHED;
            [self goToFinishedTestView];
            break;
        default:
            NSLog(@"Invalid ear state! This shouldn't be possible.");
            break;
    }
    
}
-(void)saveFrequencyData
{
    [currentCustomer addResult:[NSNumber numberWithInteger:currentDecibel] OnEar:currentEar];
    testAction = 0;
}
-(void)stopTimers
{
    [soundTimer invalidate];
    soundTimer = nil;
    
    [randomPause invalidate];
    randomPause = nil;
    
    [waitForVoiceIntroToFinishTimer invalidate];
    waitForVoiceIntroToFinishTimer = nil;
    [voicePlayer stop];
    
    [waitForVoiceToFinishTimer invalidate];
    waitForVoiceToFinishTimer = nil;
    
    [waitForVoice06 invalidate];
    waitForVoice06 = nil;
    
    [nextEarVoice invalidate];
    nextEarVoice = nil;
    
    [voiceTestIsFinished invalidate];
    voiceTestIsFinished = nil;
}

#pragma mark UIMethods
-(void)disableButton
{
    _soundButton.enabled = NO;
}
-(void)enableButton
{
    _soundButton.enabled = YES;
}
-(void)setLeftEarProgressToSoundNr:(int)soundnr;
{
    float progress = (float)soundnr / ((float)[[soundPlayer getFrequencies] count]);
    
    [_leftEarProgressBar setProgress:progress animated:YES];
}
-(void)setRightEarProgressToSoundNr:(int)soundnr;
{
    float progress = (float)soundnr / ((float)[[soundPlayer getFrequencies] count]);
    
    [_rightEarProgressBar setProgress:progress animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToFinishedTestView
{
    NSString * saveKey = [NSString stringWithFormat:@"Customer%d", [currentCustomer getCustomerId]];
    NSData *encodedCustomer = [NSKeyedArchiver archivedDataWithRootObject:currentCustomer];
    [prefs setObject:encodedCustomer forKey:saveKey];
    [prefs synchronize];
    FinishedTestViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"finishedTest"];
    currentTestState = TEST_NOT_STARTED;
    controller.customerId = self.customerId;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Ja"])
    {
        [self stopTimers];
        [soundPlayer StopSound];
        currentTestState = TEST_NOT_STARTED;
        [self.navigationController popToRootViewControllerAnimated:YES]; //To restart, go back to base controller. Might potentially need to be StartTestViewController

//        NSURL *myURL = [NSURL URLWithString:@"http://www.clicksinbricks.com/app/install.php"];
        NSURL *myURL = [NSURL URLWithString:@"cibapp://"];
        [[UIApplication sharedApplication] openURL:myURL];
    }
}

-(void)testingInProgress
{
    hearingTestState = SOUND_TEST_IS_STARTED;

    if (!pausedButtonResponse)
    {
        switch (currentTestState) {
            case TEST_NOT_STARTED:
                [self startTest];
                break;
            case TEST_IN_PROGRESS:
                [self userHeardSound];
                break;
            default:
                NSLog(@"Invalid test state. Button should be disabled when currentTestState == TEST_FINISHED");
                break;
        }
    }
    //[self goToFinishedTestView]; //TODO: move to correct position    
}

-(void)goToStartTestScreen
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"startTestScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark Buttons
- (IBAction)hearSoundButton:(id)sender {
    
    switch (hearingTestState) {
        case VOICE02_IS_PLAYING:
            break;
        case VOICE02_IS_DONE_PLAYING:
           [self activateVoice:@"voice_01"];
            hearingTestState = VOICE01_IS_PLAYING;
            waitForVoiceToFinishTimer =[NSTimer scheduledTimerWithTimeInterval:VOICE_HEARINGTEST_INTRO target:self selector:@selector(playVoice06) userInfo:nil repeats:NO];
            break;
        case BTN_PRESSED_FOR_FIRST_TIME:
            break;
        case BTN_PRESSED_NOT_FIRST_TIME_AND_VOICE01_STILL_PLAYING:
            break;
        case VOICE01_IS_PLAYING:
            break;
        case VOICE01_IS_DONE_PLAYING:
            break;
        case VOICE06_IS_PLAYING:
            break;
        case VOICE06_IS_DONE_PLAYING:
            break;
        case VOICE07_IS_PLAYING:
            break;
//        case VOICE07_IS_DONE_PLAYING:
//            break;
        case SOUND_TEST_IS_STARTED:
            [self testingInProgress];
            break;
        default:
            NSLog(@"Invalid test state. Button should be disabled when currentTestState == TEST_FINISHED");
            break;
    }
}
//When restart is pressed, cancel everything and go back to base view
- (IBAction)restartTestButton:(id)sender {
    [self stopTimers];
    [soundPlayer StopSound];
    currentTestState = TEST_NOT_STARTED;
    //Maybe add a "are you sure?" message box
//    [self.navigationController popToRootViewControllerAnimated:YES]; //To restart, go back to base controller. Might potentially need to be StartTestViewController
    [voicePlayer stop];
    [voicePlayer setVolume:0];

    voicePlayer = nil;
    [self goToStartTestScreen];
}

- (void) showAreYouSurePopup
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Webshop"
                                                      message:@"U verlaat de app en gaat naar onze shop, weet u het zeker?"
                                                     delegate:self
                                            cancelButtonTitle:@"Nee"
                                            otherButtonTitles:@"Ja", nil];
    [message show];
}

#pragma mark VoiceExplanationMethods
-(void)playHearingTestIntroVoice
{
    hearingTestState = VOICE02_IS_DONE_PLAYING;
}

-(void)playVoice06
{
    [self activateVoice:@"voice_06"];
    hearingTestState = VOICE06_IS_PLAYING;
    waitForVoice06 = [NSTimer scheduledTimerWithTimeInterval:VOICE_HEARINGTEST_LEFT_EAR target:self selector:@selector(playVoiceHearingIntro) userInfo:nil repeats:NO];
}

-(void)playVoiceHearingIntro
{
    hearingTestState = VOICE06_IS_DONE_PLAYING;
    [self testingInProgress];
}

-(void)activateVoice:(NSString *)voice
{
    NSString * stringPath;
    
    if ([voice isEqualToString:@"voice_01"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_01" ofType:@"mp3"];
    }
    if ([voice isEqualToString:@"voice_02"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_02" ofType:@"mp3"];
    }
    if ([voice isEqualToString:@"voice_04"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_04" ofType:@"mp3"];
    }
    if ([voice isEqualToString:@"voice_06"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_06" ofType:@"mp3"];
    }
    if ([voice isEqualToString:@"voice_07"])
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"voice_07" ofType:@"mp3"];
    }
    
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    NSError *error;
    voicePlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    [voicePlayer setVolume:1];
    [voicePlayer play];
}

@end
