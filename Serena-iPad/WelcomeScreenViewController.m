//
//  WelcomeScreenViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//


#import "WelcomeScreenViewController.h"
#import "Login.h"

//#define MEASURED_DB_BTN_SCREEN_OFFSET 70

@interface WelcomeScreenViewController ()

@end

@implementation WelcomeScreenViewController

@synthesize userNameField;
@synthesize passwordField;

Login * loginHelper;
UIActivityIndicatorView *activityView;
NSTimer * idleTimer;
NSInteger offsetForKeyBoard;
bool stillEditing;
bool internet_active;
NSUserDefaults *preferences;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];

    if (alertView.tag == 1) //Success Dialog Box
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    if([title isEqualToString:@"Ja"])
    {
        [self goToMainView];
    }
    if([title isEqualToString:@"Nee"])
    {
        NSLog(@"jim");
//        [self initWelcomeScreenSettings];
    }
}
- (void) requestFinished:(ASIHTTPRequest *) request
{
    //Send finished request to the Login class, it will be handled there.
    
    FinishedRequestStatus returnStatus = [loginHelper HandleFinishedLoginRequest:request];
    //Execute action according to response.
    switch (returnStatus) {
        case RequestSuccess:
        {
            [self setThisScreenAsInitial];
            [self storeUsernameAndPassword];

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Inloggen Gelukt"
                                                            message:[NSString stringWithFormat:@"U bent nu ingelogd."]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            break;
        }
        case InvalidCredentialsError:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Inloggen Mislukt"
                                                            message:@"Gebruikersnaam of wachtwoord klopt niet. Probeer opnieuw."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        
            break;
        }
        case ConnectionError:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Inloggen Mislukt"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            break;
        }
        default:
            NSLog(@"ReturnStatus is leeg. Werkt HandleFinishedRequest wel goed?");
            break;
    }
    [activityView stopAnimating];
    [self goToMainView];
}
//Request failed: most likely, credentials have been denied
-(void) requestFailed:(ASIHTTPRequest *) request
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Inloggen Mislukt"
                                                    message:@"Gebruikersnaam of wachtwoord klopt niet. Probeer opnieuw."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [activityView stopAnimating];
    NSLog(@"Login: failed login attempt");
}

- (void)initialLoginAttempt
{
    //Try a request with stored information
    ASIHTTPRequest *request = [loginHelper CreateLoginRequestWithStoredAccount];
    if (request != nil)
    {
        [activityView startAnimating];
        [self.view addSubview:activityView];
        [request setDelegate:self];
        [request startAsynchronous];
    }
    else
    {
        //No valid stored information
        NSLog(@"No valid stored info, waiting for user input");
        
    }
}

- (void)setThisScreenAsInitial
{
    NSString *screenchoice = @"all_checks_passed";
    preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:screenchoice forKey:@"screen"];
    [preferences synchronize];    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *storedUsername = [preferences objectForKey:@"username"];
    NSString *storedPassword = [preferences objectForKey:@"password"];
    
    loginHelper = [[Login alloc] init];
    activityView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center=self.view.center;

    if (storedUsername != nil && storedPassword != nil)
    {
        [self storedLoginDataFoundPopup];
    }
    else
    {
        [self initWelcomeScreenSettings];
    }
}

- (void) initWelcomeScreenSettings
{
    [self initialLoginAttempt];
    [self ResetIdleTimer];
    self.userNameField.delegate = self;
    self.passwordField.delegate = self;
    stillEditing = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToMainView
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"startTestScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)continueButtonActivated:(id)sender {
    NSString * username = userNameField.text;
    NSString * password = passwordField.text;
    //TODO: String sanitization required here?
    
    if (![self connected]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet"
                                                        message:@"Er is geen internet."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    } else {

        if (username.length != 0 && password.length != 0)
        {
            ASIFormDataRequest *request = [loginHelper CreateLoginRequestWithUsername:username andPassword:password];
            if (request != nil)
            {
                [activityView startAnimating];
                [self.view addSubview:activityView];
                [request setDelegate:self];
                [request startAsynchronous];
            }
            else
            {
                //No valid stored information
                NSLog(@"Invalid data, could not build login request");
                
                
                
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missende gegevens"
                                                            message:@"Niet alle velden zijn ingevuld. Probeer opnieuw."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    
}

- (void)ResetIdleTimer{
    if (ACTIVE)
    {
        if (!idleTimer) {
            idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAXIDLETIMESECONDS
                                                         target:self
                                                       selector:@selector(idleTimerExceeded)
                                                       userInfo:nil
                                                        repeats:NO];
        }
        else {
            if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < MAXIDLETIMESECONDS-1.0) {
                [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:MAXIDLETIMESECONDS]];
            }
        }
    }
    
}
//Triggers when view has been idling for too long
- (void)idleTimerExceeded {
    if (self.isViewLoaded && self.view.window)
    {
        idleTimer = nil;
        [self ResetIdleTimer];
        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"advertisement"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}
//Reset timer when something happens in UI
- (UIResponder *)nextResponder {
    [self ResetIdleTimer];
    return [super nextResponder];
}

#pragma mark Screen offset functions
//-(void)textFieldDidBeginEditing:(UITextField *)sender
//{
//    if (offsetForKeyBoard == MEASURED_DB_BTN_SCREEN_OFFSET)
//    {
//        stillEditing = true;
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:0.3];
//        CGRect rect = scroll.frame;
//
//        rect.origin.y -= offsetForKeyBoard;
//        rect.size.height += offsetForKeyBoard;
//        scroll.frame = rect;
//        offsetForKeyBoard = 0;
//    }
//
//    stillEditing = true;
//    [UIView commitAnimations];
//}
//
//-(void)textFieldDidEndEditing:(UITextField *)sender
//{
//    [self setOffsetScreenBack];
//}
//
//- (void)keyboardDidHide:(NSNotification *)aNotification {
//    stillEditing = false;
//    [self setOffsetScreenBack];
//}
//
//-(void)setOffsetScreenBack
//{
//    if (offsetForKeyBoard == 0 && stillEditing == false)
//    {
//        [UIView beginAnimations:nil context:NULL];
//        [UIView setAnimationDuration:0.3];
//        CGRect rect = scroll.frame;
//        rect.origin.y += MEASURED_DB_BTN_SCREEN_OFFSET;
//        rect.size.height -= MEASURED_DB_BTN_SCREEN_OFFSET;
//        scroll.frame = rect;
//        [UIView commitAnimations];
//        offsetForKeyBoard = MEASURED_DB_BTN_SCREEN_OFFSET;
//    }
//}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

- (void) storedLoginDataFoundPopup
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Inloggegevens"
                                                      message:@"Opgeslagen inloggegevens zijn gevonden, deze gebruiken?"
                                                     delegate:self
                                            cancelButtonTitle:@"Nee"
                                            otherButtonTitles:@"Ja", nil];
    [message show];
}

- (void) storeUsernameAndPassword
{
    preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:userNameField.text forKey:@"username"];
    [preferences setObject:passwordField.text forKey:@"password"];
    [preferences synchronize];
}

@end
