//
//  Customer.h
//  Serena-iPad
//
//  Created by Student on 08-05-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PlaySound.h"
#import "Database.h"

@interface Customer : NSObject
- (id) initwithCustomerName:(NSString*)name SurName:(NSString*)surname Email:(NSString*)email andDateOfBirth:(NSDate*)dateOfBirth andWantsToReceiveNewsLetter:(Boolean)wantToReceiveNewsLetter;
-(void)setIsComplete:(Boolean)iscomplete;
-(NSString*)getCustomerName;
-(NSString*)getCustomerSurName;
-(NSInteger)getCustomerId;
-(NSString*)getCustomerEmail;
-(NSDate*)getCreationDate;
-(NSDate*)getDateOfBirth;
-(Boolean)hasBeenUploaded;
-(void)setHasBeenUploaded:(Boolean)uploaded;
-(NSInteger)getFletcherIndex;
-(NSMutableArray*)getLeftEarResults;
-(NSMutableArray*)getRightEarResults;
-(void)addResult:(NSNumber*)result OnEar:(EarToPlaySoundOn)ear;
-(Boolean)isComplete;
-(Boolean)getWantToReceiveNewsLetter;
@end
