//
//  SavedTestViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "SavedTestViewController.h"

@interface SavedTestViewController ()

@end

@implementation SavedTestViewController

AVAudioPlayer *voicePlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAXIDLETIMESECONDS
                                                 target:self
                                               selector:@selector(idleTimerExceeded)
                                               userInfo:nil
                                                repeats:NO];

    
    [self ResetIdleTimer];
    [self activateVoice:@"voice_15"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)goBackToMainScreen
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"startTestScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark Timer functions
NSTimer* idleTimer;
- (void)ResetIdleTimer{
        if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < MAXIDLETIMESECONDS-1.0) {
            [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:MAXIDLETIMESECONDS]];
    }
}
//Triggers when view has been idling for too long
- (void)idleTimerExceeded {
    
    if (self.isViewLoaded && self.view.window)
    {
        idleTimer = nil;
        [self ResetIdleTimer];
        [self goBackToMainScreen];
    }
    
}
//Reset timer when something happens in UI
- (UIResponder *)nextResponder {
    [self ResetIdleTimer];
    return [super nextResponder];
}

#pragma mark VoiceExplanationMethods
-(void)activateVoice:(NSString *)voice
{
    NSString * stringPath;
    stringPath= [[NSBundle mainBundle]pathForResource:@"voice_15" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    NSError *error;
    voicePlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    [voicePlayer setVolume:1];
    [voicePlayer play];
}
@end
