//
//  CalibrationViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "CalibrationViewController.h"
#import "Frequency.h"
#define LEFT 0
#define RIGHT 1
#define INFINITE -1
#define MEASURED_DB_BTN_SCREEN_OFFSET 240

@interface CalibrationViewController ()
{
    AVAudioPlayer *avPlayer;
    NSInteger numFreqs;
    NSInteger rightleft;
    NSInteger decibel;
    BOOL buttonToggled;
    NSMutableArray *frequencys;
    NSInteger CurrentFrequency;
    float volume;
    Database *database;
    NSInteger offsetForKeyBoard;
    NSUserDefaults *preferences;
}
@end

@implementation CalibrationViewController
@synthesize decibelValueField;
@synthesize measuredDBField;
@synthesize volumeMultiplierField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setThisScreenAsInitial
{
    NSString *screenchoice = @"calibration_passed";
    preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:screenchoice forKey:@"screen"];
    [preferences synchronize];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self setThisScreenAsInitial];  //ONLY FOR DEBUG

    preferences = [NSUserDefaults standardUserDefaults];
    NSString *storedCalibration= [preferences objectForKey:@"stored_calibration"];
    
    if (storedCalibration != nil)
    {
        [self goToWelcomeView];
    }
    else
    {
        [self initSounds];
        [self setFreq:0];
        rightleft = LEFT;
        [self updateText];
        buttonToggled = false;
        CurrentFrequency = 0;
        self.measuredDBField.delegate = self;
        offsetForKeyBoard = MEASURED_DB_BTN_SCREEN_OFFSET;        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Buttons
- (IBAction)onoffButton:(id)sender {
    
    if (!buttonToggled) {
        [sender setTitle:@"On" forState:UIControlStateNormal];
        buttonToggled = YES;
        [self playFreq];
    }
    else {
        [sender setTitle:@"Off" forState:UIControlStateNormal];
        buttonToggled = NO;
        [self stopSound];
    }
}

- (IBAction)setDecibelButton:(id)sender {
    [self setDecibel:[decibelValueField.text intValue]];
    [self playFreq];
    [self.view endEditing:YES]; //Hide keyboard
}

- (IBAction)setMultiplierButton:(id)sender {

    NSCharacterSet *_NumericOnly = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ±~`,/?<>\';:[]{}+_)(*&^%$#@!"];
    NSCharacterSet *myStringSet = [NSCharacterSet characterSetWithCharactersInString:volumeMultiplierField.text];
    
    if ([_NumericOnly isSupersetOfSet: myStringSet])
    {
        //String entirely contains decimal numbers only.
        [self showInvalidMultiplierPopup ];
    }
    else
    {
        [self setMult:[volumeMultiplierField.text floatValue]];
        [self playFreq];
        [self.view endEditing:YES]; //Hide keyboard
    }
}

- (IBAction)calculateMultiplierButton:(id)sender {

    NSString *measured = measuredDBField.text;
    if (measured.length>0)
    {
        NSCharacterSet *_NumericOnly = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ±~`,/?<>\';:[]{}+_)(*&^%$#@!"];
        NSCharacterSet *myStringSet = [NSCharacterSet characterSetWithCharactersInString:measuredDBField.text];
        
        if ([_NumericOnly isSupersetOfSet: myStringSet])
        {
            //String entirely contains decimal numbers only.
            [self showInvalidMultiplierPopup ];
        }
        else
        {
            float dBuit, mult, vol, deling, power;
            dBuit = [measured floatValue];
            vol = volume;
            deling = (float) dBuit/20;
            power = (float) pow(10, deling);
            mult = vol/power;
            volumeMultiplierField.text = [NSString stringWithFormat:@"%f",mult];
            [self setMult:mult];
            [self playFreq];
        }
    }
    else
    {
        [self showInvalidValuePopup];
    }
    
    [self setOffsetScreenBack];

    [self.view endEditing:YES]; //Hide keyboard
}

- (IBAction)saveButton:(id)sender {
    [self saveNext];
}

#pragma mark Ppopups
- (void) showInvalidValuePopup
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Calibratie"
                                                    message:@"Dit is een ongeldige waarde voor het aantal decibels."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) showInvalidMultiplierPopup
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Calibratie"
                                                    message:@"Dit is een ongeldige waarde voor de multiplier."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) showCalibrationFinishedPopup
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Calibratie" message:@"Calibratie voltooid. Vul uw naam (of initialen) in om te bevestigen." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.keyboardType = UIKeyboardTypeDefault;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    // Shutdown after calibration completed
//    NSLog(@"  %@ ",[[alertView textFieldAtIndex:0] text]);
    
    if ([[alertView textFieldAtIndex:0] text].length <= 0)
    {
        [self showCalibrationFinishedPopupNoNameEntered];
    }
    else
    {
        [database insertSettings:@"Calibration_name":[[alertView textFieldAtIndex:0] text]];
        [self storeCalibration:[[alertView textFieldAtIndex:0] text]];
        [self setThisScreenAsInitial];
            exit(0);
//        [self goToPersonDataView];  //for debugging
    }
}

- (void) showCalibrationFinishedPopupNoNameEntered
{
    [self showCalibrationFinishedPopup];
}

#pragma mark Business logic
- (void) saveCalibrationData
{
    for (int n=0; n<=numFreqs; n++)
    {
        Frequency *frequency = [frequencys objectAtIndex:n];
        [database updateFreq:frequency];
    }
    
    [self showCalibrationFinishedPopup];
}

- (void) saveNext
{
    if(CurrentFrequency==numFreqs)
    {
        if(rightleft==LEFT)
        {
            rightleft = RIGHT;
            CurrentFrequency = 0;
            [self setFreq:CurrentFrequency];
            [self playFreq];
        }
        else
        {
            [self stopSound];
            [self saveCalibrationData];
        }
    }
    else
    {
        CurrentFrequency++;
        [self setFreq:CurrentFrequency];
        [self playFreq];
        
        Frequency *frequency = [frequencys objectAtIndex:CurrentFrequency];
        NSMutableString* aString = [NSMutableString stringWithFormat:@"Frequency: %d", [frequency getFreq]];
        [aString appendFormat:@" Hz"];
        self.freqencyLabel.text = aString;
    }
}

- (void) setMult:(float)mult
{
    Frequency *frequency = [frequencys objectAtIndex:CurrentFrequency];
    
    if(rightleft==LEFT)
    {
        [frequency setLeftMultiplier:mult];
    }
    else
    {
        [frequency setRightMultiplier:mult];
    }
    
    [self setDecibel:decibel];
}

- (void) updateText
{
    self.decibelValueField.text =[NSString stringWithFormat:@"%d",decibel];
    self.hardwareVolumeField.text = [NSString stringWithFormat:@"%1.10f",volume];
    
    Frequency *frequency = [frequencys objectAtIndex:CurrentFrequency];
    NSMutableString* aString = [NSMutableString stringWithFormat:@"Frequency: %d", [frequency getFreq]];
    [aString appendFormat:@" Hz"];
    self.freqencyLabel.text = aString;
    
    if (rightleft==LEFT)
        self.leftrightLabel.text = @"Linker oor";
    else
        self.leftrightLabel.text = @"Rechter oor";
}

- (void) stopSound
{
    [avPlayer stop];
}

- (void) setCurrentFreqSound:(NSInteger)calibrationres
{
    NSString *stringPath;
    
    if(calibrationres == 2130968584)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_cal_500_01" ofType:@"mp3"];
    }
    if(calibrationres == 2130968581)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_cal_1000_01" ofType:@"mp3"];
    }
    if(calibrationres == 2130968582)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_cal_2000_02" ofType:@"mp3"];
    }
    if(calibrationres == 2130968583)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_cal_4000_04" ofType:@"mp3"];
    }
    if(calibrationres == 2130968585)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_cal_7500_08" ofType:@"mp3"];
    }
    
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    NSError *error;
    avPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
}

- (void) playFreq
{
    [avPlayer stop];
    [self updateText];
    
    if (buttonToggled)
    {
        Frequency *freqency = [[Frequency alloc] init];
        freqency = [frequencys objectAtIndex:CurrentFrequency];
        [self setCurrentFreqSound:freqency.getCalibrationRes];
        
        [avPlayer setNumberOfLoops:INFINITE];
        
        if (rightleft==RIGHT)
        {
            [avPlayer setPan:1.0]; //Right ear
        }
        else
        {
            [avPlayer setPan:-1.0];  //Left ear
        }
        [avPlayer setVolume:volume];
        [avPlayer play];
    }
}

- (void) initSounds
{
    Frequency *tempFreq = [[Frequency alloc] init];
    database = [[Database alloc] init];
    
    NSString *stringPath = [[NSBundle mainBundle]pathForResource:@"freq_500_01" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    
    NSError *error;
    avPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    
    numFreqs = -1;
    frequencys = [[NSMutableArray alloc] init];
    for (int n = 0; n<10; n++)
    {
        tempFreq = [database getFreqOfObject:n];
        
        if ([tempFreq getEnabled] == 1)
        {
            numFreqs++;
            [frequencys addObject:tempFreq];
        }
    }
}

- (void) setDecibel:(NSInteger)Decibel
{
    float berekend = 0;
    float deling = (float) Decibel/20;
    float power = (float) pow(10, deling);
    
    Frequency *frequency = [frequencys objectAtIndex:CurrentFrequency];
    
    if (rightleft==LEFT)
    {
        berekend = (float) frequency.getLeftMultiplier * power;
    }
    else
    {
        berekend = (float) frequency.getRightMultiplier * power;
    }
    
    volume = berekend;
    decibel = Decibel;
    [self playFreq];
}

- (void) setFreq:(NSInteger)freq
{
    CurrentFrequency = freq;
    [self setDecibel:35];
    Frequency *freqency = [[Frequency alloc] init];
    
    if (rightleft==LEFT)
    {
        freqency = [frequencys objectAtIndex:CurrentFrequency];
        [volumeMultiplierField setText:[NSString stringWithFormat:@"%f", [freqency getLeftMultiplier]]];
    }
    else
    {
        freqency = [frequencys objectAtIndex:CurrentFrequency];
        [volumeMultiplierField setText:[NSString stringWithFormat:@"%f", [freqency getLeftMultiplier]]];
    }
}

#pragma mark for debugging
-(void)goToWelcomeView
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"welcomeScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark Advertisement
//Triggers when view has been idling for too long
//- (void)idleTimerExceeded {
//    idleTimer = nil;
//    [self ResetIdleTimer];
//    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"advertisement"];
//    [self.navigationController pushViewController:controller animated:YES];
//    
//}
//
////Reset timer when something happens in UI
//- (UIResponder *)nextResponder {
//    [self ResetIdleTimer];
//    return [super nextResponder];
//}
//
//NSTimer* idleTimer;
//- (void)ResetIdleTimer{
//    if (!idleTimer) {
//        idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAXIDLETIMESECONDS
//                                                     target:self
//                                                   selector:@selector(idleTimerExceeded)
//                                                   userInfo:nil
//                                                    repeats:NO];
//    }
//    else {
//        if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < MAXIDLETIMESECONDS-1.0) {
//            [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:MAXIDLETIMESECONDS]];
//        }
//    }
//    
//}

#pragma mark Screen offset functions
-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    CGRect rect = scrollView.frame;
    
    if ([sender isEqual:measuredDBField])
    {
        rect.origin.y -= offsetForKeyBoard;
        rect.size.height += offsetForKeyBoard;
        offsetForKeyBoard= 0;
        scrollView.frame = rect;
    }
    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)sender
{
    [self setOffsetScreenBack];
}

-(void)setOffsetScreenBack
{
    if (offsetForKeyBoard == 0)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        CGRect rect = scrollView.frame;
        rect.origin.y += MEASURED_DB_BTN_SCREEN_OFFSET;
        rect.size.height -= MEASURED_DB_BTN_SCREEN_OFFSET;
        scrollView.frame = rect;
        [UIView commitAnimations];
        offsetForKeyBoard = MEASURED_DB_BTN_SCREEN_OFFSET;
    }    
}

- (void) storeCalibration:(NSString *)name
{
    preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:name forKey:@"stored_calibration"];
    [preferences synchronize];
}
@end
