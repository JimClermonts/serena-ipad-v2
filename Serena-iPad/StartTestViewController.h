//
//  StartTestViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdleTimer.h"
@interface StartTestViewController : UIViewController
- (IBAction)continueButton:(id)sender;
- (IBAction)shopButton:(id)sender;

@end
