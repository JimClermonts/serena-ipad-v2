//
//  CalibrationViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "IdleTimer.h"
#import "Database.h"
#import "Frequency.h"

@interface CalibrationViewController : UIViewController <UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView *scrollView;
}
- (IBAction)onoffButton:(id)sender;
- (IBAction)setDecibelButton:(id)sender;
- (IBAction)setMultiplierButton:(id)sender;
- (IBAction)calculateMultiplierButton:(id)sender;
- (IBAction)saveButton:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *freqencyLabel;
@property (weak, nonatomic) IBOutlet UITextField *decibelValueField;
@property (weak, nonatomic) IBOutlet UILabel *leftrightLabel;
@property (weak, nonatomic) IBOutlet UILabel *hardwareVolumeField;
@property (weak, nonatomic) IBOutlet UITextField *measuredDBField;
@property (weak, nonatomic) IBOutlet UITextField *volumeMultiplierField;
@end
