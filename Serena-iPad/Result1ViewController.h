//
//  Result1ViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface Result1ViewController : UIViewController
- (IBAction)closeButton:(id)sender;
- (IBAction)webshopButton:(id)sender;

@end
