//
//  Result2ViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 27-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "Result2ViewController.h"

@interface Result2ViewController ()

@end

@implementation Result2ViewController

AVAudioPlayer *voicePlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self activateVoice:@"voice_10"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToSavedTest
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"savedTestScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)webshopButton:(id)sender {
    [self showAreYouSurePopup];
}

- (IBAction)closeButton:(id)sender {
    [self goToSavedTest];    
}

- (void) showAreYouSurePopup
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Webshop"
                                                      message:@"U verlaat de app en gaat naar onze shop, weet u het zeker?"
                                                     delegate:self
                                            cancelButtonTitle:@"Nee"
                                            otherButtonTitles:@"Ja", nil];
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Ja"])
    {
//        NSURL *myURL = [NSURL URLWithString:@"http://www.clicksinbricks.com/app/install.php"];
        NSURL *myURL = [NSURL URLWithString:@"cibapp://"];
        [[UIApplication sharedApplication] openURL:myURL];
    }
}

#pragma mark VoiceExplanationMethods
-(void)activateVoice:(NSString *)voice
{
    NSString * stringPath;
    stringPath= [[NSBundle mainBundle]pathForResource:@"voice_10" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    NSError *error;
    voicePlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    [voicePlayer setVolume:1];
    [voicePlayer play];
}
@end
