//
//  TokenCheckViewController.m
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "TokenCheckViewController.h"

@interface TokenCheckViewController ()

@end

@implementation TokenCheckViewController

UIActivityIndicatorView *activityView;
Login* loginHelper;
NSUserDefaults *preferences;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) //Success Dialog Box
    {
        [self setThisScreenAsInitial];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}

- (void) requestFinished:(ASIHTTPRequest *) request
{
    //Send finished request to the Login class, it will be handled there.
    
    FinishedRequestStatus returnStatus = [loginHelper HandleFinishedCheckTokenRequest:request withToken:_AATField.text];
    //Execute action according to response.
    switch (returnStatus) {
        [activityView stopAnimating];
        case RequestSuccess:
        {
            [self setThisScreenAsInitial]; //Token activatie gelukt, save dit naar user prefs
            [self storeAAT];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tablet geactiveerd" message:@"De token is geaccepteerd. Deze tablet is nu geactiveerd." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert setTag:1];
            [alert show];
            
            break;
        }
        case InvalidCredentialsError:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Token check mislukt"
                                                            message:@"Het ingevulde token klopte niet. Probeer opnieuw."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert setTag:1];
            [alert show];
            
            break;
        }
        case ConnectionError:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Token check mislukt"
                                                            message:@"Er is een fout met het verbinden met de server."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            break;
        }
        default:
            NSLog(@"ReturnStatus is leeg. Werkt HandleFinishedRequest wel goed?");
            break;
    }
    
}

-(void) requestFailed:(ASIHTTPRequest *) request
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Token Check Mislukt"
                                                    message:[NSString stringWithFormat:@"Token niet geaccepteerd. Probeer overnieuw."]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


- (void)setThisScreenAsInitial
{
    NSString *screenchoice = @"aat_passed";
    preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:screenchoice forKey:@"screen"];
    [preferences synchronize];
}

-(void)goToCalibration
{
    UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"calibrationScreen"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    preferences = [NSUserDefaults standardUserDefaults];
    NSString *storedAAT= [preferences objectForKey:@"stored_aat"];

    if (storedAAT != nil)
    {
        [self goToCalibration]; //Token activatie gelukt, save dit naar user prefs
    }
    else
    {
        activityView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityView.center=self.view.center;
//        [self setThisScreenAsInitial];  //ONLY FOR DEBUG
        //    [self ResetIdleTimer];
        loginHelper = [[Login alloc] init];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkTokenButton:(id)sender {
    NSString * token = _AATField.text;
//    NSLog(@"Token: %@\n", token);
    //TODO: String sanitization required here?
    if (token.length != 0 )
    {
        if (![self connected])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Internet"
                                                            message:@"Er is geen internet."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        else
        {
            ASIFormDataRequest *request = [loginHelper CreateCheckTokenRequestWithToken:token];
            if (request != nil)
            {
                [activityView startAnimating];
                [self.view addSubview:activityView];
                [request setDelegate:self];
                [request startAsynchronous];
            }
            else
            {
                //No valid stored information
                NSLog(@"Invalid data, could not build login request");
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missende gegevens"
                                                        message:@"Niet alle velden zijn ingevuld. Probeer opnieuw."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

//NSTimer* idleTimer;
//- (void)ResetIdleTimer{
//    if (ACTIVE)
//    {
//        if (!idleTimer) {
//            idleTimer = [NSTimer scheduledTimerWithTimeInterval:MAXIDLETIMESECONDS
//                                                         target:self
//                                                       selector:@selector(idleTimerExceeded)
//                                                       userInfo:nil
//                                                        repeats:NO];
//        }
//        else {
//            if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < MAXIDLETIMESECONDS-1.0) {
//                [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:MAXIDLETIMESECONDS]];
//            }
//        }
//    }
//    
//}
////Triggers when view has been idling for too long
//- (void)idleTimerExceeded {
//    if (self.isViewLoaded && self.view.window)
//    {
//        idleTimer = nil;
//        [self ResetIdleTimer];
//        UIViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"advertisement"];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    
//}
//Reset timer when something happens in UI
//- (UIResponder *)nextResponder {
//    [self ResetIdleTimer];
//    return [super nextResponder];
//}

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus == NotReachable);
}

- (void) storeAAT
{
    preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:_AATField.text forKey:@"stored_aat"];
    [preferences synchronize];
}

@end
