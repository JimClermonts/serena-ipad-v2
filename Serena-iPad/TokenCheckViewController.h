//
//  TokenCheckViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IdleTimer.h"
#import "Login.h"
#import "Reachability.h"

@interface TokenCheckViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *AATField;
- (IBAction)checkTokenButton:(id)sender;

@end
