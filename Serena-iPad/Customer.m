//
//  Customer.m
//  Serena-iPad
//
//  Created by Student on 08-05-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "Customer.h"

@implementation Customer
NSInteger _customerId;
NSString* _name;
NSString* _surName;
NSString* _email;
NSDate* _creationDate;
NSDate* _dateOfBirth;
Boolean _uploaded;
Boolean _completed = false;
NSMutableArray* _leftEarResults;
NSMutableArray* _rightEarResults;
Boolean _wantsToReceiveNewsletter;
Database *db;


NSUserDefaults *prefs;
- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeInteger:_customerId forKey:@"customerId"];
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_surName forKey:@"surName"];
    [encoder encodeObject:_email forKey:@"email"];
    [encoder encodeObject:_creationDate forKey:@"creationDate"];
    [encoder encodeObject:_dateOfBirth forKey:@"dateOfBirth"];
    [encoder encodeBool:_uploaded forKey:@"uploaded"];
    [encoder encodeObject:_leftEarResults forKey:@"leftEarResults"];
    [encoder encodeObject:_rightEarResults forKey:@"rightEarResults"];
    [encoder encodeBool:_wantsToReceiveNewsletter forKey:@"wantsToReceiveNewsletter"];
    [encoder encodeBool:_completed forKey:@"completed"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        _customerId = [decoder decodeIntegerForKey:@"customerId"];
        _name = [decoder decodeObjectForKey:@"name"];
        _surName = [decoder decodeObjectForKey:@"surName"];
        _email = [decoder decodeObjectForKey:@"email"];
        _creationDate = [decoder decodeObjectForKey:@"creationDate"];
        _dateOfBirth= [decoder decodeObjectForKey:@"dateOfBirth"];
        _uploaded = [decoder decodeBoolForKey:@"uploaded"];
        _leftEarResults= [decoder decodeObjectForKey:@"leftEarResults"];
        _rightEarResults= [decoder decodeObjectForKey:@"rightEarResults"];
        _wantsToReceiveNewsletter = [decoder decodeBoolForKey:@"wantsToReceiveNewsletter"];
        _completed = [decoder decodeBoolForKey:@"completed"];
        db = [[Database alloc]init];
    }
    return self;
}

- (id) initwithCustomerName:(NSString*)name SurName:(NSString*)surname Email:(NSString*)email andDateOfBirth:(NSDate*)dateOfBirth andWantsToReceiveNewsLetter:(Boolean)wantToReceiveNewsLetter
{
    prefs = [NSUserDefaults standardUserDefaults];
    _name = name;
    _surName = surname;
    _email = email;
    _dateOfBirth = dateOfBirth;
    _creationDate = [NSDate date];//Now
    _leftEarResults = [[NSMutableArray alloc] init];
    _rightEarResults = [[NSMutableArray alloc] init];
    _uploaded = NO;
    _wantsToReceiveNewsletter = wantToReceiveNewsLetter;
    
    db = [[Database alloc]init];
    if ([prefs valueForKey:@"highestId"] != nil)
    {
        NSInteger lastId = [prefs integerForKey:@"highestId"];
        _customerId = lastId + 1;
    }
    else
    {
        _customerId = 0;
    }
    [prefs setInteger:_customerId forKey:@"highestId"];
    return self;
}
-(NSString*)getCustomerName
{
    return _name;
}
-(NSString*)getCustomerSurName
{
    return _surName;
}
-(NSInteger)getCustomerId
{
    return _customerId;
}
-(NSString*)getCustomerEmail
{
    return _email;
}
-(NSDate*)getCreationDate
{
    return _creationDate;
}
-(NSDate*)getDateOfBirth
{
    return _dateOfBirth;
}
-(Boolean)hasBeenUploaded
{
    return _uploaded;
}
-(void)setHasBeenUploaded:(Boolean)uploaded
{
    _uploaded = uploaded;
}
-(NSInteger)getFletcherIndex
{
    
        NSInteger sumRight = 0;
        NSInteger sumLeft =0;
    
        for (NSInteger i = 0; i < [_rightEarResults count]; i++)
        {
            if([[db getFreqOfObject:i] getFreq] == 1000 || [[db getFreqOfObject:i] getFreq] == 2000 || [[db getFreqOfObject:i] getFreq] == 4000)
            {
                sumRight += [_rightEarResults[i] integerValue];
                sumLeft += [_leftEarResults[i] integerValue];
            }
        }
        
        sumRight = sumRight/3;
        sumLeft = sumLeft/3;
        
        if (sumRight >= sumLeft)
        {
            return sumRight;
        }
        else
        {
            return sumLeft;
        }
    
}

-(NSMutableArray*)getLeftEarResults
{
    return _leftEarResults;
}
-(NSMutableArray*)getRightEarResults
{
    return _rightEarResults;
}
-(void)addResult:(NSNumber*)result OnEar:(EarToPlaySoundOn)ear
{
    switch (ear)
    {
        case LEFT_EAR:
            [_leftEarResults addObject:result];
            break;
        case RIGHT_EAR:
            [_rightEarResults addObject:result];
            break;
        default:
            break;
    }
}
-(void)setIsComplete:(Boolean)iscomplete
{
    _completed = iscomplete;
}
-(Boolean)isComplete
{
    //return ([_leftEarResults count] >= 10 && [_rightEarResults count]>= 10);
    return _completed;
}
-(Boolean)getWantToReceiveNewsLetter
{
    return _wantsToReceiveNewsletter;
}
@end
