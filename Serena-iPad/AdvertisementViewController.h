//
//  AdvertisementViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvertisementViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *ReturnButton;
@property (weak, nonatomic) IBOutlet UIImageView *AdImageView;
- (IBAction)ReturnButtonTouched:(id)sender;

@end
