//
//  FinishedTestViewController.h
//  Serena-iPad
//
//  Created by Jim Clermonts on 20-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "IdleTimer.h"
#import "Customer.h"
#import "Login.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "Reachability.h"

@interface FinishedTestViewController : UIViewController
- (IBAction)shopButton:(id)sender;
@property(nonatomic) NSInteger customerId;
@end
