//
//  PlaySound.m
//  Serena-iPad
//
//  Created by Student on 23-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import "PlaySound.h"
#import <AVFoundation/AVFoundation.h>

@implementation PlaySound
AVAudioPlayer* audioPlayer;
NSMutableArray* frequencies;
Database * frequencyDatabase;
NSInteger maxFreqId = -1; // TODO: properly define this
-(NSInteger) getMaxFreqId
{
    return maxFreqId;
}
- (id) init
{
    self = [super init];
    
    //TODO: We need to add a correct audio url
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"freq_500_01" ofType:@"mp3"]];
	NSError * AudioError;
    
	
    
    //Alloc audio player
	audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&AudioError];
	audioPlayer.numberOfLoops = -1;
    
    //Init database class and frequency list for sound playing
    frequencyDatabase = [[Database alloc] init];
    frequencies = [[NSMutableArray alloc] init];
    
    //Populate frequency list
    //Note: if a sound is "disabled", it will not be added to the list, and maxfreqid will be corrected
    for (NSInteger i = 0; i < 9; i++)
    {
        Frequency* currentFrequency = [frequencyDatabase getFreqOfObject:i];
        if ([currentFrequency getEnabled] == 1)
        {
            [frequencies addObject:currentFrequency];
            maxFreqId++;
        }
    }
    //maxFreqId = [frequencies count] - 1;
	
	if (audioPlayer == nil)
    {
        NSLog(@"The following error has occured while starting AVAudioPlayer: %@", [AudioError description]);
    }

    return self;
}
- (void) SetupSoundWithFrequency:(int)freq WithDecibel:(float)decibel OnEar:(EarToPlaySoundOn)ear
{
    
    
    if (audioPlayer != nil)
    {
        [self StopSound];
        if (freq <= maxFreqId)
        {
            Frequency *frequencyFromId = [frequencies objectAtIndex:freq];
       
            [self setCurrentFreqSound:[frequencyFromId getCalibrationRes]];
            
            [audioPlayer setNumberOfLoops:0];
            
            
            
            [audioPlayer setPan:ear];
            
            //Code to change decibel to volume and set volume (came from android version) Is this correct???
            float volume = [self getCorrectVolumeFromDecibel:decibel FreqId:freq OnEar:ear];
            [audioPlayer setVolume:volume];
            
       
            
            //PrepareToPlay will populate buffer and make sure sound plays as fast as possible.
            if(![audioPlayer prepareToPlay])
            {
                NSLog(@"Warning: Preparation of audio failed. Sound might not play on time or at all.");
            }
        }
        else
        {
            NSLog(@"Error: Invalid sound chosen!");
        }
    }
    else
    {
        NSLog(@"No audioPlayer defined!");
    }
}
- (float)getCorrectVolumeFromDecibel:(NSInteger)decibel FreqId:(NSInteger)freqId OnEar:(EarToPlaySoundOn)ear
{
    
    
    float berekend = 0;
    
    float deling = (float) decibel/20;
    float power = (float) pow(10, deling);
    
    NSLog(@"berekend: %1.10f",deling);
    NSLog(@"berekend: %1.10f",power);
    
    Frequency *frequency = [frequencies objectAtIndex:freqId];
    
    switch (ear) {
        case LEFT_EAR:
            berekend = (float) frequency.getLeftMultiplier * power;
            NSLog(@"berekend left : %f",frequency.getLeftMultiplier);
            break;
        case RIGHT_EAR:
            berekend = (float) frequency.getRightMultiplier * power;
            NSLog(@"berekend right: %f",frequency.getRightMultiplier);
            break;
        default:
            berekend = 1.0;
            break;
            
    }
    
    NSLog(@"berekend: %1.10f",berekend);
    return berekend;
    
    
}

- (void) PlaySoundNow
{
    if (audioPlayer != nil)
    {
        
        [audioPlayer play];
    }
    else
    {
        NSLog(@"No audioPlayer defined!");
    }
}
- (void) StopSound
{
    if (audioPlayer != nil && [audioPlayer isPlaying])
    {
        
        [audioPlayer stop];
    }
    else
    {
        NSLog(@"No audio playing!");
    }
}
- (void) setCurrentFreqSound:(NSInteger)calibrationres
{
    NSString *stringPath;
    
    if(calibrationres == 2130968584)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_500_01" ofType:@"mp3"];
    }
    if(calibrationres == 2130968581)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_1000_01" ofType:@"mp3"];
    }
    if(calibrationres == 2130968582)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_2000_02" ofType:@"mp3"];
    }
    if(calibrationres == 2130968583)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_4000_04" ofType:@"mp3"];
    }
    if(calibrationres == 2130968585)
    {
        stringPath= [[NSBundle mainBundle]pathForResource:@"freq_7500_08" ofType:@"mp3"];
    }
    
    NSURL *url = [NSURL fileURLWithPath:stringPath];
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:&error];
    
    if (error != nil)
    {
        NSLog(@"Error: %@", [error description]);
    }
}
- (NSMutableArray*)getFrequencies
{
    return frequencies;
}
@end
