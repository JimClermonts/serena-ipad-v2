//
//  PlaySound.h
//  Serena-iPad
//
//  Created by Student on 23-04-13.
//  Copyright (c) 2013 Serena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Database.h"
#import "Frequency.h"

@interface PlaySound : NSObject
typedef enum
{
    LEFT_EAR = -1,
    RIGHT_EAR = 1,
    BOTH_EARS = 0
} EarToPlaySoundOn;

- (id) init;

//Setup sound first so lag is as small as possible when playing sound.
- (void) SetupSoundWithFrequency:(int)freq WithDecibel:(float)decibel OnEar:(EarToPlaySoundOn)ear;
- (void) PlaySoundNow;
- (void) StopSound;
- (NSInteger) getMaxFreqId;
- (NSMutableArray*)getFrequencies;

@end
